﻿namespace PK.Robocode.Core.Strategies
{
    using System;
    public interface IStrategy
    {
        AbstractCoreRobot Robot { get; }

        double Priority { get; set; }

        Func<IStrategy, double> AdjustPriority { get; set; }

        Action<IStrategy> AdjustStrategy { get; set; }

        bool IsActiv { get; }

        bool Debug { get; set; }

        void Init();

        void Do();

        void UpdatePriority();

        void UpdateStrategy();

        event EventHandler Activated;

        event EventHandler Deactivated;

        void Activate();

        void Deactivate();

        string GetName();
    }
}