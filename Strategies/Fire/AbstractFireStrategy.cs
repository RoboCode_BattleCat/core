﻿namespace PK.Robocode.Core.Strategies.Fire
{
    using System;
    using System.Drawing;
    using Extra.Cache;
    using global::Robocode;
    using PK.Robocode.Core.Model;
    using Utils;

    public abstract class AbstractFireStrategy : AbstractStrategy, IFireStrategy
    {
        private DateTime lastTimeToggledFire = DateTime.Now;

        private IDependendPropertyCache<double, IEnemy> aimingAngleCache;

        protected AbstractFireStrategy(AbstractCoreRobot robot)
            : base(robot)
        {
            this.aimingAngleCache = new DependendOncePerTick<double, IEnemy>(enemy => this.CalcAimingAngle(enemy));

            this.DetermineFirePower = enemy => Math.Min(400 / MathUtils.DistanceBetween(enemy.Position, Robot.Position), 3);
#if DEBUG
            this.Robot.KeyTyped += (sender, args) =>
                {
                    if (args.KeyChar.ToString().ToLower().Equals("g")
                        && DateTime.Now - this.lastTimeToggledDebug > TimeSpan.FromSeconds(1))
                    {
                        this.Debug = !this.Debug;
                        this.lastTimeToggledDebug = DateTime.Now;
                    }
                    else if (args.KeyChar.ToString().ToLower().Equals("f")
                             && DateTime.Now - this.lastTimeToggledFire > TimeSpan.FromSeconds(1))
                    {
                        this.IsFireEnabled = !this.IsFireEnabled;
                        this.lastTimeToggledFire = DateTime.Now;
                    }
                };

            this.Robot.Draw += this.PrintAimingAngle;
            this.Robot.Draw += this.PrintGunHeading;
#endif
        }

        public double BearingTolerance { get; set; } = 5.0;

        public Func<IRobotMeta, double> DetermineFirePower { get; set; }

        public bool IsFireEnabled { get; set; } = true;

        public abstract double CalcAimingAngle(IEnemy enemy, int tickOffset = 0);

        public double GetAimingAngle(IEnemy enemy, int tickOffset = 0) => this.aimingAngleCache.Value(enemy);

        public override void Do()
        {
            var enemy = this.Robot.MostPriorityEnemy;

            if (enemy == null)
            {
                return;
            }

            var degree = this.GetAimingAngle(enemy);
            var firePower = this.DetermineFirePower(enemy);

            this.Robot.SetTurnGunRight(degree);
            this.Robot.SetFire(firePower, this.BearingTolerance);
        }

#if DEBUG
        private void PrintAimingAngle(object sender, IGraphics graphics)
        {
            if (!this.Debug || !this.IsActiv)
            {
                return;
            }

            var enemy = this.Robot.MostPriorityEnemy;
            var aimingAngle = this.GetAimingAngle(enemy);
            var point = MathUtils.GetWallPoint(
                450,
                this.Robot.DistanceFarestCorner,
                aimingAngle,
                this.Robot.Position,
                this.Robot.BattleFieldWidth,
                this.Robot.BattleFieldHeight);

            graphics.DrawLine(new Pen(Color.FromArgb(128, Color.Red)), this.Robot.Position, point);
        }

        private void PrintGunHeading(object sender, IGraphics graphics)
        {
            if (!this.Debug || !this.IsActiv)
            {
                return;
            }

            var point = MathUtils.GetWallPoint(
                450,
                this.Robot.DistanceFarestCorner,
                this.Robot.GunHeading,
                this.Robot.Position,
                this.Robot.BattleFieldWidth,
                this.Robot.BattleFieldHeight);

            graphics.DrawLine(new Pen(Color.FromArgb(128, Color.WhiteSmoke)), this.Robot.Position, point);
        }
#endif
    }
}