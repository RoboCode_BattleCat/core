﻿namespace PK.Robocode.Core.Strategies.Fire
{
    using System;

    using PK.Robocode.Core.Model;

    public interface IFireStrategy : IStrategy
    {
        bool IsFireEnabled { get; set; }

        Func<IRobotMeta, double> DetermineFirePower { get; set; }

        double GetAimingAngle(IEnemy enemy, int tickOffset = 0);
    }
}