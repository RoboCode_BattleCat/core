﻿namespace PK.Robocode.Core.Strategies.Movement
{
    using System.Drawing;

    using global::Robocode;
    using Utils;

    public abstract class AbstractMovementStrategy : AbstractStrategy, IMovementStrategy
    {
        protected RectangleF fieldRect;

        protected double wallMargin = 18;

        protected AbstractMovementStrategy(AbstractCoreRobot robot)
            : base(robot)
        {
            //this.Robot.Init += (sender, args) => { this.WallMargin = this.wallMargin; };

#if DEBUG
            //this.Robot.KeyTyped += (sender, args) =>
            //    {
            //        if (args.KeyChar.ToString().ToLower().Equals("m")
            //            && DateTime.Now - this.lastTimeToggledDebug > TimeSpan.FromSeconds(1))
            //        {
            //            this.Debug = !this.Debug;
            //            this.lastTimeToggledDebug = DateTime.Now;
            //        }
            //    };
#endif
        }

        //public double WallMargin
        //{
        //    get
        //    {
        //        return this.wallMargin;
        //    }
        //    set
        //    {
        //        if (this.Robot.IsRunning)
        //        {
        //            this.fieldRect = new RectangleF(
        //                (float)value,
        //                (float)value,
        //                (float)(this.Robot.BattleFieldWidth - value * 2),
        //                (float)(this.Robot.BattleFieldHeight - value * 2));
        //        }

        //        this.wallMargin = value;
        //    }
        //}

        public double WallStick { get; set; } = 160;

        public double WallSmoothing(double angle, int orientation)
        {
            return this.WallSmoothing(this.Robot.Position, angle, orientation);
        }

        public double WallSmoothing(PointF position, double angle, int orientation)
        {
            while (!this.fieldRect.Contains(MathUtils.Project(position, angle, this.WallStick)))
            {
                angle += orientation * 0.05;
            }

            return angle;
        }

#if DEBUG
        private static readonly Color color = Color.FromArgb(150, Color.WhiteSmoke);

        private static readonly SolidBrush brush = new SolidBrush(color);

        private static readonly Pen pen = new Pen(brush);

        private static readonly Color frontColor = Color.FromArgb(150, Color.LimeGreen);

        private static readonly SolidBrush frontBrush = new SolidBrush(frontColor);

        private static readonly Pen frontPen = new Pen(frontBrush);

        private static readonly Color backgroundColor = Color.FromArgb(50, Color.DarkGray);

        private static readonly SolidBrush backgroundBrush = new SolidBrush(backgroundColor);

        private static readonly Font font = new Font(FontFamily.GenericMonospace, 5f);

        private static readonly int margin = 5;

        private static readonly int fontSize = 15;

        private static readonly int backgroundX = 10;

        private static readonly int backgroundY = 10;

        private static readonly int backgroundWidth = 150;

        private static readonly int backgroundHeight = 90;

        protected override void Print(IGraphics graphics)
        {
            if (!this.Debug || !this.IsActiv)
            {
                return;
            }

            this.DrawDistanceMarkers(graphics);
            this.DrawDistances(graphics);

            graphics.DrawRectangle(new Pen(Color.FromArgb(75, Color.Red)), this.fieldRect);
        }

        private void DrawDistanceMarkers(IGraphics graphics)
        {
            graphics.DrawLine(frontPen, this.Robot.Position, this.Robot.FrontWall);
            graphics.DrawLine(pen, this.Robot.Position, this.Robot.LeftWall);
            graphics.DrawLine(pen, this.Robot.Position, this.Robot.RightWall);
            graphics.DrawLine(pen, this.Robot.Position, this.Robot.BackWall);
        }

        private void DrawDistances(IGraphics graphics)
        {
            graphics.FillRectangle(backgroundBrush, backgroundX, backgroundY, backgroundWidth, backgroundHeight);

            var i = 1;
            graphics.DrawString(
                $"Front: {this.Robot.DistanceFrontWall}",
                font,
                brush,
                new Point(backgroundX + margin, backgroundY + backgroundHeight - (margin + fontSize) * i++));

            graphics.DrawString(
                $"Back: {this.Robot.DistanceBackWall}",
                font,
                brush,
                new Point(backgroundX + margin, backgroundY + backgroundHeight - (margin + fontSize) * i++));

            graphics.DrawString(
                $"Left: {this.Robot.DistanceLeftWall}",
                font,
                brush,
                new Point(backgroundX + margin, backgroundY + backgroundHeight - (margin + fontSize) * i++));

            graphics.DrawString(
                $"Right: {this.Robot.DistanceRightWall}",
                font,
                brush,
                new Point(backgroundX + margin, backgroundY + backgroundHeight - (margin + fontSize) * i++));
        }
#endif
    }
}