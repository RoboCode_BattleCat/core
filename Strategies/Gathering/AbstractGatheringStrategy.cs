﻿namespace PK.Robocode.Core.Strategies.Gathering
{
    using System.Drawing;

    using global::Robocode;
    using Utils;

    public abstract class AbstractGatheringStrategy : AbstractStrategy, IGatheringStrategy
    {
        protected AbstractGatheringStrategy(AbstractCoreRobot robot)
            : base(robot)
        {
#if DEBUG
            //this.Robot.KeyTyped += (sender, args) =>
            //    {
            //        if (args.KeyChar.ToString().ToLower().Equals("r")
            //            && DateTime.Now - this.lastTimeToggledDebug > TimeSpan.FromSeconds(1))
            //        {
            //            this.Debug = !this.Debug;
            //            this.lastTimeToggledDebug = DateTime.Now;
            //        }
            //    };

            //robot.Draw += this.Draw;
#endif
        }

#if DEBUG
        public void Draw(object sender, IGraphics graphics)
        {
            if (!this.Debug || !this.IsActiv)
            {
                return;
            }

            var points = new PointF[Definitions.RadarAngle];
            points[0] = new PointF((float)this.Robot.X, (float)this.Robot.Y);

            for (var i = 1; i < Definitions.RadarAngle; i++)
            {
                points[i] = MathUtils.PointOnCircle(
                    Definitions.RadarDistance,
                    Definitions.RadarDegreeCorrection - Definitions.RadarAngle / 2 - (float)this.Robot.RadarHeading + i,
                    points[0]);
            }

            graphics.FillPolygon(brush, points);
        }
#endif
#if DEBUG
        private static readonly Color color = Color.FromArgb(75, Color.LightSeaGreen);

        private static readonly SolidBrush brush = new SolidBrush(color);
#endif
    }
}