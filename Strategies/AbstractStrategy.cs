﻿namespace PK.Robocode.Core.Strategies
{
    using System;
    public abstract class AbstractStrategy : IStrategy
    {
#if DEBUG
        protected DateTime lastTimeToggledDebug = DateTime.Now;
#endif

        protected AbstractStrategy(AbstractCoreRobot robot)
        {
            this.Robot = robot;
#if DEBUG
            this.Robot.Draw += this.Draw;
#endif
        }

        public event EventHandler Activated;
        public event EventHandler Deactivated;

        public bool Debug { get; set; } = false;

        public bool IsActiv { get; protected set; }
        public double Priority { get; set; }
        public AbstractCoreRobot Robot { get; protected set; }
        public Func<IStrategy, double> AdjustPriority { get; set; }
        public Action<IStrategy> AdjustStrategy { get; set; }

        //protected GatherService GatherService { get; private set; }

        public virtual void Do()
        {
            // do nothing by default
        }

        public virtual void Init()
        {
            // do nothing by default
        }

        public virtual string GetName()
        {
            return this.GetType().Name;
        }

#if DEBUG
        private void Draw(object sender, IGraphics graphics)
        {
            if (this.Debug && this.IsActiv)
            {
                this.Print(graphics);
            }
        }

        protected virtual void Print(IGraphics graphics)
        {
            // do nothing by default
        }
#endif

        public void UpdatePriority()
        {
            var invoke = this.AdjustPriority?.Invoke(this);
            if (invoke != null)
            {
                this.Priority = (double)invoke;
            }
        }

        public void UpdateStrategy()
        {
            this.AdjustStrategy?.Invoke(this);
        }

        public void Activate()
        {
            if (this.IsActiv)
            {
                return;
            }

            this.IsActiv = true;
            this.Activated?.Invoke(this, null);
        }

        public void Deactivate()
        {
            if (!this.IsActiv)
            {
                return;
            }

            this.IsActiv = false;
            this.Deactivated?.Invoke(this, null);
        }
    }
}