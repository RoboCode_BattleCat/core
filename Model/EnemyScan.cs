﻿namespace PK.Robocode.Core.Model
{
    using System;
    using System.Drawing;
    using global::Robocode;
    using Utils;

    public class EnemyScan : EnemyMeta, ICloneable
    {
        private EnemyScan()
        {
        }

        public long ScannedTick { get; set; }

        public override object Clone()
        {
            return new EnemyScan
            {
                Name = this.Name,
                Energy = this.Energy,
                Heading = this.Heading,
                HeadingRadians = this.HeadingRadians,
                Velocity = this.Velocity,
                Position = Position,
                ScannedTick = this.ScannedTick
            };
        }

        public static EnemyScan FromEvent(ScannedRobotEvent e, AbstractCoreRobot robot)
        {
            var angle = MathUtils.DegreeToRadian((robot.Heading + e.Bearing));

            var scan = new EnemyScan
            {
                Name = e.Name,
                ScannedTick = robot.Time,
                Energy = e.Energy,
                Heading = e.Heading,
                HeadingRadians = e.HeadingRadians,
                Velocity = e.Velocity,
                Position = new PointF(
                    (float)(robot.X + Math.Sin(angle) * e.Distance),
                    (float)(robot.Y + Math.Cos(angle) * e.Distance))
            };

            return scan;
        }
    }
}