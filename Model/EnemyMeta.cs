﻿using System;
using PK.Robocode.Core.Extra;
using PK.Robocode.Core.Extra.Cache;
using PK.Robocode.Core.Utils;

namespace PK.Robocode.Core.Model
{
    public class EnemyMeta : RobotMeta, IEnemyMeta
    {
        protected override void InitPropertyCache()
        {
            base.InitPropertyCache();

            this.BearingCache = PropertyCacheFactory.Create(() => MathUtils.Bearing(Position, AbstractCoreRobot.Instance.Position));
            this.BearingRadiansCache = PropertyCacheFactory.Create(() => MathUtils.DegreeToRadian(Bearing));
            this.AbsoluteBearingCache = PropertyCacheFactory.Create(() => MathUtils.AbsoluteBearing(Position, AbstractCoreRobot.Instance.Position));
            this.AbsoluteBearingRadiansCache = PropertyCacheFactory.Create(() => MathUtils.DegreeToRadian(Bearing));
            this.LateralVelocityCache = PropertyCacheFactory.Create(() => AbstractCoreRobot.Instance.Velocity * Math.Sin(Bearing));
            this.DistanceCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(Position, AbstractCoreRobot.Instance.Position));
        }

        public double AbsoluteBearing => this.AbsoluteBearingCache.Value;

        public double AbsoluteBearingRadians => this.AbsoluteBearingRadiansCache.Value;

        public double Bearing => this.BearingCache.Value;

        public double BearingRadians => this.BearingRadiansCache.Value;

        public double Distance => this.DistanceCache.Value;

        public bool IsSentryRobot { get; protected set; }

        public double LateralVelocity => this.LateralVelocityCache.Value;

        #region PropertyCache
        protected IPropertyCache<double> LateralVelocityCache { get; set; }
        protected IPropertyCache<double> DistanceCache { get; set; }
        protected IPropertyCache<double> BearingCache { get; set; }
        protected IPropertyCache<double> BearingRadiansCache { get; set; }
        protected IPropertyCache<double> AbsoluteBearingCache { get; set; }
        protected IPropertyCache<double> AbsoluteBearingRadiansCache { get; set; }
        #endregion PropertyCache
    }
}
