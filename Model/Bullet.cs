﻿using System.Drawing;
using Base = Robocode;

namespace PK.Robocode.Core.Model
{
    public class Bullet : AbstractBullet
    {
        public Bullet(IRobot firedBy = null, IRobot target = null) : base(firedBy, target)
        {
        }

        private Base.Bullet BaseBullet { get; set; }

        public static Bullet FromBaseBullet(Base.Bullet bullet, IRobot firedBy = null, IRobot target = null)
        {
            return bullet == null
                       ? null
                       : new Bullet(firedBy)
                       {
                           Heading = bullet.Heading,
                           HeadingRadians = bullet.HeadingRadians,
                           IsActive = bullet.IsActive,
                           Power = bullet.Power,
                           Target = target,
                           FiredPosition = new PointF((float)bullet.X, (float)bullet.Y)
                       };
        }

        public override bool Equals(object obj)
        {
            return this.BaseBullet.Equals(obj);
        }

        public override int GetHashCode()
        {
            return this.BaseBullet.GetHashCode();
        }

        public bool Equals(Base.Bullet bullet)
        {
            return this.BaseBullet.Equals(bullet);
        }
    }
}