﻿using System.Collections.Generic;
using System.Drawing;

namespace PK.Robocode.Core.Model
{
    public interface IEnemy : IRobot, IEnemyMeta
    {
        bool IsDead { get; }

        double Priority { get; }

        bool HasFired { get; }

        EnemyScan LastScan { get; }
        IList<EnemyScan> Scans { get; }

        // TOOD
        //PointF GetPosition(long time);

        PointF GetFuturePosition(long time);
        double DistanceTo(PointF point);

        long LastTimeScanned { get; }
    }
}
