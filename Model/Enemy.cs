﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using PK.Robocode.Core.Extensions;
using PK.Robocode.Core.Extra;
using PK.Robocode.Core.Extra.Cache;
using PK.Robocode.Core.Utils;
using Robocode;

namespace PK.Robocode.Core.Model
{
    public class Enemy : IEnemy
    {
        public Enemy(AbstractCoreRobot robot, EnemyScan firstScan, bool isSentryRobot = false)
        {
            this.LastScan = firstScan;
            this.Scans.Add(firstScan);

            this.Robot = robot;

            this.Name = firstScan.Name;
            this.IsSentryRobot = isSentryRobot;

            this.InitPropertyCache();

            this.Robot.RobotDeath += Death;
            this.Robot.EnemyScanned += GotScanned;
        }

        public long LastTimeScanned { get; private set; } = int.MinValue;

        private void GotScanned(object sender, EnemyScan enemyScan)
        {
            if (enemyScan.Name != this.Name)
            {
                return;
            }

            var now = this.Robot.Time;
            if (this.LastTimeScanned == now)
            {
                return;
            }

            this.LastTimeScanned = now;

            this.PreviousScan = this.LastScan;
            this.LastScan = enemyScan;

            this.Scans.Add(enemyScan);
            this.Scanned?.Invoke(this, enemyScan);

            if (this.HasFired)
            {
                var bulletPower = this.PreviousScan.Energy - this.LastScan.Energy;

                if (bulletPower == 0)
                    return;

                var bullet = new Bullet(this, this.Robot)
                {
                    Heading = this.Bearing,
                    HeadingRadians = this.BearingRadians,
                    IsActive = true,
                    Power = bulletPower,
                    FiredTick = Robot.Time,
                    FiredPosition = new PointF(this.Position.X, this.Position.Y)
                };

                this.Bullets.Add(bullet);

                this.BulletScanned?.Invoke(this, bullet);
            }
        }

        public double DistanceTo(PointF point)
        {
            return MathUtils.DistanceBetween(this.Position, point);
        }

        #region Properties

#if DEBUG
        public bool Debug { get; set; }
#endif

        protected AbstractCoreRobot Robot { get; private set; }

        public bool IsSentryRobot { get; protected set; }

        public string Name { get; protected set; }

        public double AbsoluteBearing => this.AbsoluteBearingCache.Value;

        public double AbsoluteBearingRadians => this.AbsoluteBearingRadiansCache.Value;

        public double Bearing => this.BearingCache.Value;

        public double BearingRadians => this.BearingRadiansCache.Value;

        public double Distance => this.DistanceCache.Value;

        public double Energy => this.GetValue(ls => ls.Energy);

        public double Heading => this.GetValue(ls => ls.Heading);

        public double HeadingRadians => this.GetValue(ls => ls.HeadingRadians);

        public double Velocity => this.GetValue(ls => this.IsDisabled ? 0.0 : ls.Velocity);

        public double TurnSpeed => this.TurnSpeedCache.Value;

        public bool HasFired => this.HasFiredCached.Value;

        public double DistanceBackWall => this.DistanceBackWallCache.Value;

        public double DistanceFrontWall => this.DistanceFrontWallCache.Value;

        public double DistanceLeftWall => this.DistanceLeftWallCache.Value;

        public double DistanceRightWall => this.DistanceRightWallCache.Value;

        public PointF FarestCorner => this.FarestCornerCache.Value;

        public double DistanceFarestCorner => this.DistanceFarestCornerCache.Value;

        public PointF FrontWall => this.BackWallCache.Value;

        public PointF BackWall => this.BackWallCache.Value;

        public PointF LeftWall => this.LeftWallCache.Value;

        public PointF RightWall => this.RightWallCache.Value;

        public PointF[] HitBox => this.HitBoxCache.Value;

        public PointF Position => this.PositionCache.Value;

        protected IList<Bullet> Bullets { get; private set; } = new RingBuffer<Bullet>(1024);

        public IEnumerable<IBullet> ActiveBullets => this.ActiveBulletsCache.Value;

        public IEnumerable<IBullet> BulletsHistory => this.BulletsHistoryCache.Value;

        public double Priority => PriorityCache.Value;
        public double LateralVelocity => LateralVelocityCache.Value;

        #region PropertyCache
        protected IPropertyCache<double> PriorityCache { get; set; }
        protected IPropertyCache<double> LateralVelocityCache { get; set; }
        protected IPropertyCache<double> DistanceCache { get; set; }
        protected IPropertyCache<double> BearingCache { get; set; }
        protected IPropertyCache<double> BearingRadiansCache { get; set; }
        protected IPropertyCache<double> AbsoluteBearingCache { get; set; }
        protected IPropertyCache<double> AbsoluteBearingRadiansCache { get; set; }
        protected IPropertyCache<PointF> PositionCache { get; set; }
        protected IPropertyCache<PointF> FarestCornerCache { get; set; }
        protected IPropertyCache<PointF> FrontWallCache { get; set; }
        protected IPropertyCache<PointF> BackWallCache { get; set; }
        protected IPropertyCache<PointF> LeftWallCache { get; set; }
        protected IPropertyCache<PointF> RightWallCache { get; set; }
        protected IPropertyCache<double> DistanceFarestCornerCache { get; set; }
        protected IPropertyCache<double> DistanceFrontWallCache { get; set; }
        protected IPropertyCache<double> DistanceBackWallCache { get; set; }
        protected IPropertyCache<double> DistanceLeftWallCache { get; set; }
        protected IPropertyCache<double> DistanceRightWallCache { get; set; }
        protected IPropertyCache<PointF[]> HitBoxCache { get; set; }
        protected IPropertyCache<bool> IsDisabledCache { get; set; }
        protected IPropertyCache<double> TurnSpeedCache { get; set; }
        protected IPropertyCache<bool> HasFiredCached { get; set; }
        protected IPropertyCache<List<Bullet>> ActiveBulletsCache { get; set; }
        protected IPropertyCache<List<Bullet>> BulletsHistoryCache { get; set; }
        #endregion

        public IList<EnemyScan> Scans { get; } = new RingBuffer<EnemyScan>(1024);
        public EnemyScan LastScan { get; private set; }
        public EnemyScan PreviousScan { get; private set; }

        public bool IsDead { get; protected set; }
        public bool IsDisabled => IsDisabledCache.Value;

        #endregion

        #region events
        public event EventHandler<EnemyScan> Scanned;
        public event EventHandler<Bullet> BulletScanned;
        #endregion

        public PointF GetFuturePosition(long time)
        {
            return MathUtils.GetFuturePosition(this.Position, this.Velocity, this.Heading, time - this.LastScan.ScannedTick);
        }

        private void Death(object sender, RobotDeathEvent e)
        {
            if (e.Name != this.Name)
            {
                return;
            }

            this.IsDead = true;
        }

        private void InitPropertyCache()
        {
            this.PositionCache = PropertyCacheFactory.Create(() => GetCurrentValue(
                ls => ls.Position,
                () => MathUtils.GetFuturePosition(LastScan, Robot.Time - LastScan.ScannedTick)));

            this.BearingCache = PropertyCacheFactory.Create(() => GetCurrentValue(
                ls => ls.Bearing,
                () => MathUtils.Bearing(Robot.Position, Position)));

            this.IsDisabledCache = PropertyCacheFactory.Create(() => this.Energy == 0);
            this.TurnSpeedCache = PropertyCacheFactory.Create(() => MathUtils.TurnSpeed(this.Velocity));
            this.LateralVelocityCache = PropertyCacheFactory.Create(() => Robot.Velocity * Math.Sin(Bearing));
            this.BearingRadiansCache = PropertyCacheFactory.Create(() => GetCurrentValue(
                ls => ls.BearingRadians,
                () => MathUtils.DegreeToRadian(Bearing)));

            this.AbsoluteBearingCache = PropertyCacheFactory.Create(() => GetCurrentValue(
                ls => ls.AbsoluteBearing,
                () => MathUtils.AbsoluteBearing(Robot.Position, Position)));

            this.AbsoluteBearingRadiansCache = PropertyCacheFactory.Create(() => GetCurrentValue(
                ls => ls.AbsoluteBearingRadians,
                () => MathUtils.DegreeToRadian(Bearing)));

            this.FarestCornerCache = PropertyCacheFactory.Create(() => new PointF(
                (float)(Position.X < Robot.BattleFieldWidth / 2 ? Robot.BattleFieldWidth : 0),
                (float)(Position.Y < Robot.BattleFieldHeight / 2 ? Robot.BattleFieldHeight : 0)));

            this.DistanceCache = PropertyCacheFactory.Create(() => GetCurrentValue(
                ls => ls.Distance,
                () => MathUtils.DistanceBetween(Position, Robot.Position)));

            this.ActiveBulletsCache = PropertyCacheFactory.Create(() => this.Bullets.Where(b =>
                MathUtils.IsWithinBattlefield(b.CurrentPosition, Robot.BattleFieldWidth, Robot.BattleFieldHeight)).ToList());

            this.BulletsHistoryCache = PropertyCacheFactory.Create(() => this.Bullets.Where(b =>
                MathUtils.IsWithinBattlefield(b.CurrentPosition, Robot.BattleFieldWidth, Robot.BattleFieldHeight)).ToList());

            this.DistanceFarestCornerCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(this.FarestCorner, this.Position));
            this.FrontWallCache = PropertyCacheFactory.Create(() => MathUtils.GetWallPoint(450, this.DistanceFarestCorner, this.Heading, this.Position, Robot.BattleFieldWidth, Robot.BattleFieldHeight));
            this.LeftWallCache = PropertyCacheFactory.Create(() => MathUtils.GetWallPoint(360, this.DistanceFarestCorner, this.Heading, this.Position, Robot.BattleFieldWidth, Robot.BattleFieldHeight));
            this.RightWallCache = PropertyCacheFactory.Create(() => MathUtils.GetWallPoint(540, this.DistanceFarestCorner, this.Heading, this.Position, Robot.BattleFieldWidth, Robot.BattleFieldHeight));
            this.BackWallCache = PropertyCacheFactory.Create(() => MathUtils.GetWallPoint(270, this.DistanceFarestCorner, this.Heading, this.Position, Robot.BattleFieldWidth, Robot.BattleFieldHeight));

            this.DistanceFrontWallCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(this.Position, this.FrontWall) - Definitions.CollissionBoxWidth / 2f);
            this.DistanceBackWallCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(this.Position, this.BackWall) - Definitions.CollissionBoxWidth / 2f);
            this.DistanceLeftWallCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(this.Position, this.LeftWall) - Definitions.CollissionBoxWidth / 2f);
            this.DistanceRightWallCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(this.Position, this.RightWall) - Definitions.CollissionBoxWidth / 2f);

            this.HasFiredCached = PropertyCacheFactory.Create(() => this.HasBulletFired());
            this.HitBoxCache = PropertyCacheFactory.Create(() => MathUtils.GetHitbox(this));
            this.PriorityCache = PropertyCacheFactory.Create(() => Robot.DetermineEnemyPriority(this));
        }

        private bool HasBulletFired()
        {
            var lastScan = this.LastScan;

            if (lastScan == null || this.PreviousScan == null)
            {
                return false;
            }

            /*
             *  => How can I detect when an enemy has fired?
             *
             *  There is no direct way to detect when an enemy fired, but you can deduce it by monitoring the enemy energy drop.
             *  A drop between 0.1 and 3 usually means that it fired a bullet (there can be other reasons, such as a low energy bullet hit or a wall hit).
             *  Wall hits are (more or less) detectable as well. A deceleration > 2 means the bot hit a wall (or another bot). A deceleration <= 2 may be
             *  simple a bot hitting the brakes, or hitting a wall at velocity = 2, but since hitting a wall at that speed won't cause any damage, you
             *  can ignore that. AdvancedRobots take abs(velocity) / 2 - 1 (Never < 0) damage when hitting a wall, so by detecting (significant)
             *  wall-hits and adjusting the enemy drop accordingly, wall hits can be filtered out most of the time. This method fails when the enemy
             *  hits another robot.
             */

            var bulletPower = this.PreviousScan.Energy - lastScan.Energy;

            var closestBullet = this.Robot.ActiveBullets.Any()
                ? this.Robot.ActiveBullets.MinBy(ab => MathUtils.DistanceBetween(ab.CurrentPosition, this.Position))
                : null;

            var distance = closestBullet == null ? 0 : MathUtils.DistanceBetween(closestBullet.CurrentPosition, this.Position);

            double relativBulletPower = bulletPower;

            if (closestBullet != null && distance <= closestBullet.Velocity * 3)
            {
                relativBulletPower -= MathUtils.BulletDamage(closestBullet.Power);
            }

            return relativBulletPower <= Definitions.MaxFirePower && relativBulletPower >= Definitions.MinFirePower;
        }

        private T GetValue<T>(Func<EnemyScan, T> func)
        {
            return this.LastScan == null
                ? default(T)
                : func(this.LastScan);
        }

        private T GetCurrentValue<T>(Func<EnemyScan, T> func, Func<T> cache, int tickOffset = 2)
        {
            return this.Robot.Time - this.LastScan.ScannedTick < tickOffset
                ? this.GetValue(func)
                : cache();
        }
    }
}
