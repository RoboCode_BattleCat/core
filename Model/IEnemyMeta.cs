namespace PK.Robocode.Core.Model
{
    public interface IEnemyMeta : IRobotMeta
    {
        bool IsSentryRobot { get; }
        double Distance { get; }
        double Bearing { get; }
        double BearingRadians { get; }
        double AbsoluteBearing { get; }
        double AbsoluteBearingRadians { get; }
        double LateralVelocity { get; }
    }
}
