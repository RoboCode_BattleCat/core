﻿namespace PK.Robocode.Core.Model
{
    public class RobotStatistic
    {
        public string Name { get; set; }

        public double Accurency => this.FireCount == 0 ? 0 : this.BulletHitCount / this.FireCount * 100;

        public double FireCount { get; set; }

        public double BulletHitCount { get; set; }

        public double HitByBulletCount { get; set; }

        public double BulletMissedCount { get; set; }

        public double BulletHitBullet { get; set; }

        public double DamageTaken { get; set; }

        public double DamgeDealed { get; set; }
    }
}