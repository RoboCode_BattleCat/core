﻿namespace PK.Robocode.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using Extra;
    using Extra.Cache;
    using Utils;

    public abstract class AbstractBullet : IBullet
    {
        public AbstractBullet(IRobot firedBy, IRobot target)
        {
            this.FiredBy = firedBy;
            this.Target = target;

            this.FiredTick = AbstractCoreRobot.Instance.Time;

            this.VelocityCache = PropertyCacheFactory.Create(() => MathUtils.BulletVelocity(this.Power), true);
            this.MaxEscapeAngleCache = PropertyCacheFactory.Create(() => Math.Asin(8 / this.Velocity), true);
            this.CurrentPositionCache = PropertyCacheFactory.Create(() => this.GetFuturePosition(AbstractCoreRobot.Instance.Time));
            this.DistanceTravaledCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(this.FiredPosition, this.CurrentPosition));
        }

        private IPropertyCache<double> VelocityCache { get; set; }
        private IPropertyCache<double> MaxEscapeAngleCache { get; set; }
        private IPropertyCache<PointF> CurrentPositionCache { get; set; }
        private IPropertyCache<double> DistanceTravaledCache { get; set; }

        public IRobot FiredBy { get; set; }

        public IRobot Target { get; set; }

        public IRobot Victim { get; set; }

        public double Heading { get; set; }

        public double HeadingRadians { get; set; }

        public bool IsActive { get; set; }

        public double Power { get; set; }

        public double Velocity => this.VelocityCache.Value;

        public long FiredTick { get; set; }

        public double FiredDistance { get; set; }

        public double MaxEscapeAngle => this.MaxEscapeAngleCache.Value;

        public PointF FiredPosition { get; set; }

        public PointF CurrentPosition => this.CurrentPositionCache.Value;

        public double DistanceTravaled => this.DistanceTravaledCache.Value;

        public PointF GetFuturePosition(long time)
        {
            return MathUtils.GetFuturePosition(this.FiredPosition, this.Velocity, this.Heading, time - this.FiredTick + 1);
        }

        public IEnumerable<PointF> GetWave(long time, int size = 20, double angle = 15.0)
        {
            for (var i = 0; i < size; i++)
            {
                yield return
                    MathUtils.GetFuturePosition(
                        this.FiredPosition,
                        this.Velocity,
                        (this.Heading - angle / 2.0) % 360.0 + angle / size * i,
                        time - this.FiredTick);
            }
        }
    }
}