﻿using System.Collections.Generic;
using System.Drawing;

namespace PK.Robocode.Core.Model
{
    public interface IBullet
    {
        IRobot FiredBy { get; set; }

        IRobot Target { get; }

        IRobot Victim { get; set; }

        double Heading { get; }

        double HeadingRadians { get; }

        bool IsActive { get; }

        double Power { get; }

        long FiredTick { get; }

        double FiredDistance { get; }

        double Velocity { get; }

        double MaxEscapeAngle { get; }

        PointF FiredPosition { get; }

        PointF CurrentPosition { get; }

        double DistanceTravaled { get; }

        PointF GetFuturePosition(long time);

        IEnumerable<PointF> GetWave(long time, int size = 20, double angle = 15.0);
    }
}
