﻿using System;
using System.Drawing;
using PK.Robocode.Core.Extra.Cache;
using PK.Robocode.Core.Utils;

namespace PK.Robocode.Core
{
    public class RobotMeta : IRobotMeta, ICloneable
    {
        public RobotMeta()
        {
            this.InitPropertyCache();
        }

        protected virtual void InitPropertyCache()
        {
            this.IsDisabledCache = PropertyCacheFactory.Create(() => this.Energy == 0);

            this.TurnSpeedCache = PropertyCacheFactory.Create(() => MathUtils.TurnSpeed(this.Velocity));

            this.HitBoxCache = PropertyCacheFactory.Create(() => MathUtils.GetHitbox(this));

            var battleFieldWidth = AbstractCoreRobot.Instance.BattleFieldWidth;
            var battleFieldHeight = AbstractCoreRobot.Instance.BattleFieldHeight;

            this.FarestCornerCache = PropertyCacheFactory.Create(() => new PointF(
                (float)(Position.X < battleFieldWidth / 2 ? battleFieldWidth : 0),
                (float)(Position.Y < battleFieldHeight / 2 ? battleFieldHeight : 0)));

            this.DistanceFarestCornerCache = PropertyCacheFactory.Create(() => MathUtils.DistanceBetween(this.FarestCorner, this.Position));
            this.FrontWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.GetWallPoint(450, this.DistanceFarestCorner, this.Heading, this.Position, battleFieldWidth, battleFieldHeight));
            this.LeftWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.GetWallPoint(360, this.DistanceFarestCorner, this.Heading, this.Position, battleFieldWidth, battleFieldHeight));
            this.RightWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.GetWallPoint(540, this.DistanceFarestCorner, this.Heading, this.Position, battleFieldWidth, battleFieldHeight));
            this.BackWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.GetWallPoint(270, this.DistanceFarestCorner, this.Heading, this.Position, battleFieldWidth, battleFieldHeight));

            this.DistanceFrontWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.DistanceBetween(this.Position, this.FrontWall) - Definitions.CollissionBoxWidth / 2f);
            this.DistanceBackWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.DistanceBetween(this.Position, this.BackWall) - Definitions.CollissionBoxWidth / 2f);
            this.DistanceLeftWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.DistanceBetween(this.Position, this.LeftWall) - Definitions.CollissionBoxWidth / 2f);
            this.DistanceRightWallCache = PropertyCacheFactory.Create(() =>
                MathUtils.DistanceBetween(this.Position, this.RightWall) - Definitions.CollissionBoxWidth / 2f);
        }

        public string Name { get; set; }
        public double Energy { get; set; }
        public double Heading { get; set; }
        public double HeadingRadians { get; set; }
        public double Velocity { get; set; }
        public double TurnSpeed => this.TurnSpeedCache.Value;
        public bool IsDisabled => IsDisabledCache.Value;

        public PointF Position { get; set; }

        public double DistanceBackWall => this.DistanceBackWallCache.Value;

        public double DistanceFrontWall => this.DistanceFrontWallCache.Value;

        public double DistanceLeftWall => this.DistanceLeftWallCache.Value;

        public double DistanceRightWall => this.DistanceRightWallCache.Value;

        public PointF FarestCorner => this.FarestCornerCache.Value;

        public double DistanceFarestCorner => this.DistanceFarestCornerCache.Value;

        public PointF FrontWall => this.BackWallCache.Value;

        public PointF BackWall => this.BackWallCache.Value;

        public PointF LeftWall => this.LeftWallCache.Value;

        public PointF RightWall => this.RightWallCache.Value;

        public PointF[] HitBox => this.HitBoxCache.Value;

        #region PropertyCach
        protected IPropertyCache<PointF> FarestCornerCache { get; set; }
        protected IPropertyCache<PointF> FrontWallCache { get; set; }
        protected IPropertyCache<PointF> BackWallCache { get; set; }
        protected IPropertyCache<PointF> LeftWallCache { get; set; }
        protected IPropertyCache<PointF> RightWallCache { get; set; }
        protected IPropertyCache<double> DistanceFarestCornerCache { get; set; }
        protected IPropertyCache<double> DistanceFrontWallCache { get; set; }
        protected IPropertyCache<double> DistanceBackWallCache { get; set; }
        protected IPropertyCache<double> DistanceLeftWallCache { get; set; }
        protected IPropertyCache<double> DistanceRightWallCache { get; set; }
        protected IPropertyCache<PointF[]> HitBoxCache { get; set; }
        protected IPropertyCache<bool> IsDisabledCache { get; set; }
        protected IPropertyCache<double> TurnSpeedCache { get; set; }
        #endregion

        public virtual object Clone()
        {
            return new RobotMeta
            {
                Name = this.Name,
                Energy = this.Energy,
                Heading = this.Heading,
                HeadingRadians = this.HeadingRadians,
                Velocity = this.Velocity,
                Position = this.Position,
            };
        }
    }
}
