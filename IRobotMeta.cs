
using System.Drawing;

public interface IRobotMeta
{
    string Name { get; }
    double Energy { get; }

    double Heading { get; }

    double HeadingRadians { get; }

    double Velocity { get; }

    double TurnSpeed { get; }

    bool IsDisabled { get; }

    PointF Position { get; }

    double DistanceFarestCorner { get; }

    PointF FarestCorner { get; }

    PointF FrontWall { get; }

    PointF LeftWall { get; }

    PointF RightWall { get; }

    PointF BackWall { get; }

    double DistanceFrontWall { get; }

    double DistanceLeftWall { get; }

    double DistanceRightWall { get; }

    double DistanceBackWall { get; }

    PointF[] HitBox { get; }
}