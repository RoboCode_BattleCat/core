﻿using PK.Robocode.Core.Utils;

namespace PK.Robocode.Core
{
    public static class Definitions
    {
        public const int CollissionBoxWidth = 36; // pixels

        public const int CollissionBoxHeight = 36; // pixels

        public const int ImageBoxWidth = 40; // pixels

        public const int ImageBoxHeight = 40; // pixels

        public const int RadarAngle = 10; // degrees;

        public const int RadarDistance = 1500; // units which are pixels in most cases

        public const int RadarDegreeCorrection = 90;

        public const double MinFirePower = 0.1;

        public const double MaxFirePower = 3.0;

        public const double MaxRadarTurnPerTurn = 45;

        public const double MaxVelocity = 8;

        public static double MaxBulletVelocity = MathUtils.BulletVelocity(MaxFirePower);

        public static double MinBulletVelocity = MathUtils.BulletVelocity(MinFirePower);
    }
}