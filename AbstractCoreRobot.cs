﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using PK.Robocode.Core.Extensions;
using PK.Robocode.Core.Extra;
using PK.Robocode.Core.Extra.Cache;
using PK.Robocode.Core.Model;
using PK.Robocode.Core.RobotExtensions;
using PK.Robocode.Core.Strategies;
using PK.Robocode.Core.Strategies.Fire;
using PK.Robocode.Core.Strategies.Gathering;
using PK.Robocode.Core.Strategies.Movement;
using PK.Robocode.Core.Utils;
using Robocode;

namespace PK.Robocode.Core
{
    public abstract class AbstractCoreRobot : AdvancedRobot, IRobot
    {
        private const string BulletScannedEventName = "BulletScanned";

        protected AbstractCoreRobot()
        {
            Instance = this;
        }

        public static AbstractCoreRobot Instance { get; private set; }

        private long fireTime;

        public bool Debug { get; set; } = false;
        public bool ShowBulletWaves { get; set; } = false;

        #region overridden Properties
        public new double BattleFieldWidth { get; private set; }
        public new double BattleFieldHeight { get; private set; }

        public new double DistanceRemaining { get; private set; }
        public new double GunHeadingRadians { get; private set; }
        public new double GunTurnRemaining { get; private set; }
        public new double GunTurnRemainingRadians { get; private set; }
        public new double RadarHeadingRadians { get; private set; }
        public new double RadarTurnRemaining { get; private set; }
        public new double RadarTurnRemainingRadians { get; private set; }
        public new double TurnRemaining { get; private set; }
        public new double TurnRemainingRadians { get; private set; }
        public new long Time { get; private set; }
        public new double Velocity { get; private set; }
        public new double GunHeading { get; private set; }
        public new double Heading { get; private set; }
        public new double HeadingRadians { get; private set; }
        public new double RadarHeading { get; private set; }
        #endregion

        #region new Properties

        public double TurnSpeed { get; private set; }
        public bool IsDisabled { get; private set; }

        public bool IsRunning { get; private set; }
        public bool PreventSelfDamageOnFire { get; set; } = true;
        public double CurrentMaxRotationRate => MathUtils.RotationRate(this.Velocity);
        public double LastTimeFired { get; protected set; }
        public double TimeUntilFireAgain { get; protected set; }
        public bool CanFire => this.Time - this.TimeUntilFireAgain > this.LastTimeFired;
        public double BattleFieldDiameter { get; private set; }
        public PointF GunAimingWall { get; private set; }
        public double DistanceGunToWall { get; private set; }

        public PointF BackWall { get; protected set; }
        public double DistanceBackWall { get; protected set; }
        public double DistanceFrontWall { get; protected set; }
        public double DistanceLeftWall { get; protected set; }
        public double DistanceRightWall { get; protected set; }
        public PointF FarestCorner { get; protected set; }
        public double DistanceFarestCorner { get; protected set; }
        public PointF FrontWall { get; protected set; }
        public PointF[] HitBox { get; protected set; }
        public PointF LeftWall { get; protected set; }
        public PointF Position { get; protected set; }
        public PointF RightWall { get; protected set; }

        public IEnumerable<IBullet> ActiveBullets => ActiveBulletCache.Value;

        private IPropertyCache<List<Model.Bullet>> ActiveBulletCache { get; set; }

        public IEnumerable<IBullet> BulletsHistory => BulletsHistoryCache.Value;

        private IPropertyCache<List<Model.Bullet>> BulletsHistoryCache { get; set; }

        protected IList<Model.Bullet> Bullets { get; } = new RingBuffer<Model.Bullet>(64);

        public IMovementStrategy CurrentMovementStrategy { get; protected set; }
        public IGatheringStrategy CurrentGatheringStrategy { get; protected set; }
        public IFireStrategy CurrentFireStrategy { get; protected set; }

        public IList<IMovementStrategy> MoveStrategies { get; } = new List<IMovementStrategy>();
        public IList<IGatheringStrategy> GatheringStrategies { get; } = new List<IGatheringStrategy>();
        public IList<IFireStrategy> FireStrategies { get; } = new List<IFireStrategy>();

        private IPropertyCache<List<IEnemy>> EnemiesAliveCache;
        public IEnumerable<IEnemy> EnemiesAlive => EnemiesAliveCache.Value;

        private IPropertyCache<List<IEnemy>> DisabledEnemiesCache;
        public IEnumerable<IEnemy> DisabledEnemies => DisabledEnemiesCache.Value;
        private IDictionary<string, IEnemy> Enemies { get; } = new Dictionary<string, IEnemy>();

        public IEnemy MostPriorityEnemy => MostPriorityEnemyCache.Value;

        private IPropertyCache<IEnemy> MostPriorityEnemyCache { get; set; }

        public IEnumerable<IBullet> ActiveEnemyBullets => ActiveEnemyBulletsCache.Value;

        private IPropertyCache<List<IBullet>> ActiveEnemyBulletsCache;

        public IList<IRobotExtension> Extensions { get; } = new List<IRobotExtension>();

        public Func<Enemy, double> DetermineEnemyPriority { get; set; } = enemy => 1 / enemy.Distance;
        #endregion

        #region events
        public event EventHandler Init;
        public event EventHandler<IMovementStrategy> MoveTick;
        public event EventHandler<IGatheringStrategy> GatherTick;
        public event EventHandler<IFireStrategy> FireTick;
        public event EventHandler<Tuple<IFireStrategy, Model.Bullet>> Fired;
        public event EventHandler Tick;
        public event EventHandler BeforeTick;
        public event EventHandler BulletCollisionDetected;
        public event EventHandler<EnemyScan> EnemyScanned;
        public event EventHandler<IEnemy> NewEnemyDetected;
        public event EventHandler<IBullet> BulletScanned;
        public event EventHandler<HitRobotEvent> HitRobot;
        public event EventHandler<HitWallEvent> HitWall;
        public event EventHandler<IBullet> HitByBullet;
        public event EventHandler<IBullet> BulletHit;
        public event EventHandler<Tuple<IBullet, IBullet>> BulletHitBullet;
        public event EventHandler<IBullet> BulletMissed;
        public event EventHandler<RoundEndedEvent> RoundEnded;
        public event EventHandler<WinEvent> Win;
        public event EventHandler<DeathEvent> Death;
        public event EventHandler<RobotDeathEvent> RobotDeath;
        public event EventHandler<SkippedTurnEvent> SkippedTurn;
        public event EventHandler<IGraphics> Draw;
        public event EventHandler<KeyEvent> KeyTyped;
        public event EventHandler<KeyEvent> KeyPressed;
        public event EventHandler<KeyEvent> KeyReleased;
        #endregion

        private void InitStrategies()
        {
            foreach (var strategy in this.FireStrategies)
            {
                strategy.Init();
            }

            foreach (var strategy in this.MoveStrategies)
            {
                strategy.Init();
            }

            foreach (var strategy in this.GatheringStrategies)
            {
                strategy.Init();
            }
        }

        public void Move()
        {
            this.CurrentMovementStrategy?.Do();
            this.MoveTick?.Invoke(this, this.CurrentMovementStrategy);
        }

        public void Gather()
        {
            this.CurrentGatheringStrategy?.Do();
            this.GatherTick?.Invoke(this, this.CurrentGatheringStrategy);
        }

        public void Shoot()
        {
            this.CurrentFireStrategy?.Do();
        }

        public void MoveTo(PointF position)
        {
            var bearingTo = MathUtils.Bearing(this.Position, position);
            var degree = MathUtils.NormalRelativeAngleDegrees(bearingTo - this.Heading);
            var distance = MathUtils.DistanceBetween(this.Position, position);

            if (Math.Abs(degree) < 90)
            {
                this.SetTurnRight(degree);
                this.SetAhead(distance);
            }
            else
            {
                this.SetTurnRight(degree - 180);
                this.SetBack(distance);
            }
        }

        public void TurnTo(double degree)
        {
            var angle = MathUtils.NormalRelativeAngleDegrees(degree - this.Heading);

            this.TurnRight(angle);
        }

        public void TurnRadarTo(double degree)
        {
            var angle = MathUtils.NormalRelativeAngleDegrees(degree - this.RadarHeading);

            this.TurnRadarRight(angle);
        }

        public void TurnGunTo(double degree)
        {
            var angle = MathUtils.NormalRelativeAngleDegrees(degree - this.GunHeading);

            this.TurnGunRight(angle);
        }

        private void UpdateProperties(StatusEvent statusEvent)
        {
            this.Time = statusEvent.Time;
            this.Velocity = statusEvent.Status.Velocity;
            this.DistanceRemaining = statusEvent.Status.DistanceRemaining;
            this.GunHeading = statusEvent.Status.GunHeading;
            this.GunHeadingRadians = statusEvent.Status.GunHeadingRadians;
            this.GunTurnRemaining = statusEvent.Status.GunTurnRemaining;
            this.GunTurnRemainingRadians = statusEvent.Status.GunTurnRemainingRadians;
            this.Heading = statusEvent.Status.Heading;
            this.HeadingRadians = statusEvent.Status.HeadingRadians;
            this.RadarHeading = statusEvent.Status.RadarHeading;
            this.RadarHeadingRadians = statusEvent.Status.RadarHeadingRadians;
            this.RadarTurnRemaining = statusEvent.Status.RadarTurnRemaining;
            this.RadarTurnRemainingRadians = statusEvent.Status.RadarTurnRemainingRadians;
            this.TurnRemaining = statusEvent.Status.TurnRemaining;
            this.TurnRemainingRadians = statusEvent.Status.TurnRemainingRadians;

            this.TurnSpeed = MathUtils.TurnSpeed(statusEvent.Status.Velocity);
            this.IsDisabled = statusEvent.Status.Energy == 0.0;

            this.Position = new PointF((float)statusEvent.Status.X, (float)statusEvent.Status.Y);
            this.FarestCorner = new PointF(
                (float)(statusEvent.Status.X < this.BattleFieldWidth / 2 ? this.BattleFieldWidth : 0),
                (float)(statusEvent.Status.Y < this.BattleFieldHeight / 2 ? this.BattleFieldHeight : 0));

            this.DistanceFarestCorner = MathUtils.DistanceBetween(this.FarestCorner, this.Position);
            this.FrontWall = MathUtils.GetWallPoint(450, this.DistanceFarestCorner, this.Heading, this.Position, this.BattleFieldWidth, this.BattleFieldHeight);
            this.LeftWall = MathUtils.GetWallPoint(360, this.DistanceFarestCorner, this.Heading, this.Position, this.BattleFieldWidth, this.BattleFieldHeight);
            this.RightWall = MathUtils.GetWallPoint(540, this.DistanceFarestCorner, this.Heading, this.Position, this.BattleFieldWidth, this.BattleFieldHeight);
            this.BackWall = MathUtils.GetWallPoint(270, this.DistanceFarestCorner, this.Heading, this.Position, this.BattleFieldWidth, this.BattleFieldHeight);
            this.GunAimingWall = MathUtils.GetWallPoint(450, this.DistanceFarestCorner, this.GunHeading, this.Position, this.BattleFieldWidth, this.BattleFieldHeight);

            this.DistanceGunToWall = MathUtils.DistanceBetween(this.Position, this.GunAimingWall) - Definitions.CollissionBoxWidth / 2f;
            this.DistanceFrontWall = MathUtils.DistanceBetween(this.Position, this.FrontWall) - Definitions.CollissionBoxWidth / 2f;
            this.DistanceLeftWall = MathUtils.DistanceBetween(this.Position, this.LeftWall) - Definitions.CollissionBoxHeight / 2f;
            this.DistanceRightWall = MathUtils.DistanceBetween(this.Position, this.RightWall) - Definitions.CollissionBoxHeight / 2f;
            this.DistanceBackWall = MathUtils.DistanceBetween(this.Position, this.BackWall) - Definitions.CollissionBoxWidth / 2f;
        }

        private void AdjustPriorities()
        {
            this.MoveStrategies.ToList().ForEach(strategy => strategy.UpdatePriority());
            this.GatheringStrategies.ToList().ForEach(strategy => strategy.UpdatePriority());
            this.FireStrategies.ToList().ForEach(strategy => strategy.UpdatePriority());
        }

        public void AdjustStrategies()
        {
            this.MoveStrategies.ToList().ForEach(strategy => strategy.UpdateStrategy());
            this.GatheringStrategies.ToList().ForEach(strategy => strategy.UpdateStrategy());
            this.FireStrategies.ToList().ForEach(strategy => strategy.UpdateStrategy());
        }
        private void SetStrategiesBasedOnPriority()
        {
            this.SetStrategyBasedOnPriority(
                this.CurrentMovementStrategy,
                this.MoveStrategies,
                strat => this.CurrentMovementStrategy = strat);

            this.SetStrategyBasedOnPriority(
                this.CurrentGatheringStrategy,
                this.GatheringStrategies,
                strat => this.CurrentGatheringStrategy = strat);

            this.SetStrategyBasedOnPriority(
                this.CurrentFireStrategy,
                this.FireStrategies,
                strat => this.CurrentFireStrategy = strat);
        }

        private void SetStrategyBasedOnPriority<T>(
            IStrategy strategy,
            IList<T> strategies,
            Action<T> setCurrentStrategyFunc) where T : IStrategy
        {
            var tmp = strategy;

            strategy = strategies.MaxBy(s => s.Priority);
            setCurrentStrategyFunc((T)strategy);

            if (tmp == strategy && strategy != null && strategy.IsActiv)
            {
                return;
            }

            tmp?.Deactivate();
            strategy?.Activate();
        }

        public override void Run()
        {
#if DEBUG
            try
            {
#endif
            this.IsRunning = true;


            this.InitBase();
            this.InitCustomEvent();
            this.Initialization();
            this.InitStrategies();

            this.Init?.Invoke(this, null);

            while (this.IsRunning)
            {
                this.BeforeTick?.Invoke(this, null);

                this.AdjustStrategies();
                this.AdjustPriorities();
                this.SetStrategiesBasedOnPriority();

                this.Tick?.Invoke(this, null);

                this.Shoot();
                this.Move();
                this.Gather();

                base.Execute();
            }
#if DEBUG
            }
            catch (InvalidCastException e)
            {
                this.Out.WriteLine(e == null ? "Exception is null" : e.Message);
            }
            catch (Exception e)
            {
                this.Out.WriteLine(e == null ? "Exception is null" : e.Message);
            }
#endif
        }

        private void InitCustomEvent()
        {
            this.AddCustomEvent(new Condition(
                BulletScannedEventName,
                c => this.Enemies.Any(e => e.Value.HasFired)));
        }

        protected virtual void Initialization()
        {
            // do nothing by default
        }

        private void InitBase()
        {
            this.BattleFieldWidth = base.BattleFieldWidth;
            this.BattleFieldHeight = base.BattleFieldHeight;
            this.BattleFieldDiameter = Math.Sqrt(Math.Pow(this.BattleFieldWidth, 2) + Math.Pow(this.BattleFieldHeight, 2));

            this.ActiveBulletCache = PropertyCacheFactory.Create(() => this.Bullets.Where(b =>
                MathUtils.IsWithinBattlefield(b.CurrentPosition, this.BattleFieldWidth, this.BattleFieldHeight)).ToList());

            this.BulletsHistoryCache = PropertyCacheFactory.Create(() => this.Bullets.Where(b =>
                MathUtils.IsWithinBattlefield(b.CurrentPosition, this.BattleFieldWidth, this.BattleFieldHeight)).ToList());

            this.MostPriorityEnemyCache = PropertyCacheFactory.Create(() => this.Enemies.Values.MaxBy(e => e.Priority));
            this.EnemiesAliveCache = PropertyCacheFactory.Create(() => this.Enemies.Values.Where(e => !e.IsDead).ToList());
            this.DisabledEnemiesCache = PropertyCacheFactory.Create(() => this.EnemiesAlive.Where(e => !e.IsDisabled).ToList());
            this.ActiveEnemyBulletsCache = PropertyCacheFactory.Create(() => this.EnemiesAlive.SelectMany(e => e.ActiveBullets).ToList());

#if DEBUG
            this.debugArea = new DebugArea(
                5,
                this.BattleFieldHeight - 5
                );
#endif
        }

        public IEnemy GetEnemy(string name)
        {
            return this.Enemies.ContainsKey(name)
                ? this.Enemies[name]
                : null;
        }

        #region events
        public override void OnScannedRobot(ScannedRobotEvent e)
        {
            var enemyScan = EnemyScan.FromEvent(e, this);

            if (!this.Enemies.ContainsKey(enemyScan.Name))
            {
                var enemy = new Enemy(this, enemyScan, e.IsSentryRobot);
                this.Enemies.Add(enemyScan.Name, enemy);
                this.NewEnemyDetected?.Invoke(this, enemy);
            }

            this.EnemyScanned?.Invoke(this, enemyScan);
        }

        public override void OnPaint(IGraphics graphics)
        {
#if DEBUG
            this.DrawStrategyInfo(graphics);
            this.DrawEnemyInfo(graphics);
            this.DrawEnemyBulletInfo(graphics);
#endif

            this.Draw?.Invoke(this, graphics);
        }

        public override void OnBattleEnded(BattleEndedEvent evnt)
        {
            this.IsRunning = false;
        }

        public override void OnHitRobot(HitRobotEvent e)
        {
            this.HitRobot?.Invoke(this, e);
        }

        public override void OnHitWall(HitWallEvent e)
        {
            this.HitWall?.Invoke(this, e);
        }

        public override void OnHitByBullet(HitByBulletEvent e)
        {
            if (string.IsNullOrEmpty(e.Bullet.Name))
            {
                return;
            }

            var bullet = Model.Bullet.FromBaseBullet(e.Bullet);
            var robot = this.GetEnemy(e.Bullet.Name);

            bullet.FiredBy = robot;
            bullet.Victim = this;

            this.HitByBullet?.Invoke(this, bullet);
        }

        public override void OnBulletHit(BulletHitEvent e)
        {
            if (string.IsNullOrEmpty(e.Bullet.Victim))
            {
                return;
            }

            var bullet = Model.Bullet.FromBaseBullet(e.Bullet);
            var robot = this.GetEnemy(e.Bullet.Victim);

            bullet.Victim = robot;
            bullet.FiredBy = this;

            this.BulletHit?.Invoke(this, bullet);
        }

        public override void OnBulletHitBullet(BulletHitBulletEvent e)
        {
            if (string.IsNullOrEmpty(e.HitBullet.Name))
            {
                return;
            }

            var bullet = Model.Bullet.FromBaseBullet(e.Bullet);
            var bullet2 = Model.Bullet.FromBaseBullet(e.HitBullet);
            var robot = this.GetEnemy(e.HitBullet.Name);

            bullet.FiredBy = this;
            bullet2.FiredBy = robot;

            this.BulletHitBullet?.Invoke(this, Tuple.Create<IBullet, IBullet>(bullet, bullet2));
        }

        public override void OnBulletMissed(BulletMissedEvent e)
        {
            var bullet = Model.Bullet.FromBaseBullet(e.Bullet);

            bullet.FiredBy = this;

            this.BulletMissed?.Invoke(this, bullet);
        }

        public override void OnRobotDeath(RobotDeathEvent e)
        {
            this.RobotDeath?.Invoke(this, e);
        }

        public override void OnRoundEnded(RoundEndedEvent e)
        {
            this.IsRunning = false;

            this.RoundEnded?.Invoke(this, e);
        }

        public override void OnWin(WinEvent e)
        {
            this.Win?.Invoke(this, e);
        }

        public override void OnSkippedTurn(SkippedTurnEvent e)
        {
            this.SkippedTurn?.Invoke(this, e);
        }

        public override void OnDeath(DeathEvent e)
        {
            this.Death?.Invoke(this, e);
        }

        public override void OnKeyTyped(KeyEvent e)
        {
            this.KeyTyped?.Invoke(this, e);
        }

        public override void OnKeyPressed(KeyEvent e)
        {
            this.KeyPressed?.Invoke(this, e);
        }

        public override void OnKeyReleased(KeyEvent e)
        {
            this.KeyReleased?.Invoke(this, e);
        }

        public override void OnCustomEvent(CustomEvent evnt)
        {
            switch (evnt.Condition.Name)
            {
                case BulletScannedEventName:
                    foreach (var enemy in this.Enemies.Values.Where(e => e.HasFired))
                    {
                        if (enemy.ActiveBullets.Any())
                        {
                            this.BulletScanned?.Invoke(this, enemy.ActiveBullets.Last());
                        }
                    }

                    break;
            }
        }

        public override void OnStatus(StatusEvent e)
        {
            this.UpdateProperties(e);
        }
        #endregion

        public double DistanceTo(PointF point)
        {
            return MathUtils.DistanceBetween(this.Position, point);
        }

        public void SetBackAsFront(double goAngle)
        {
            var angle = MathUtils.NormalRelativeAngle(goAngle - this.HeadingRadians);

            if (Math.Abs(angle) > Math.PI / 2)
            {
                if (angle < 0)
                {
                    this.SetTurnRightRadians(Math.PI + angle);
                }
                else
                {
                    this.SetTurnLeftRadians(Math.PI - angle);
                }

                this.SetBack(100);
            }
            else
            {
                if (angle < 0)
                {
                    this.SetTurnLeftRadians(-1 * angle);
                }
                else
                {
                    this.SetTurnRightRadians(angle);
                }

                this.SetAhead(100);
            }
        }

        public new void SetFire(double power)
        {
            this.SetFire(power, 1.0, true);
        }

        public Model.Bullet SetFire(double power = Definitions.MaxFirePower, double tolerance = 1.0, bool awaitTurns = true)
        {
            Model.Bullet bullet = null;

            if (!awaitTurns
                || (this.CanFire && MathUtils.IsBetween(this.GunTurnRemaining, tolerance * -1, tolerance)
                && this.CurrentFireStrategy.IsFireEnabled))
            {
                var realPower = this.PreventSelfDamageOnFire ? this.Energy < power ? this.Energy : power : power;

                base.SetFire(realPower);
                bullet = new Model.Bullet(this, this.MostPriorityEnemy)
                {
                    Heading = this.GunHeading,
                    HeadingRadians = this.GunHeadingRadians,
                    IsActive = true,
                    Power = realPower,
                    FiredPosition = new PointF((float)this.X, (float)this.Y),
                    FiredTick = Time
                };

                this.Fired?.Invoke(
                    this,
                    new Tuple<IFireStrategy, Model.Bullet>(
                        this.CurrentFireStrategy,
                        bullet));

                this.Bullets.Add(bullet);

                this.LastTimeFired = this.Time;
                this.TimeUntilFireAgain = MathUtils.TimeUntilFireAgain(realPower);
            }

            this.fireTime = this.Time + 1;

            return bullet;
        }

#if DEBUG
        private DebugArea debugArea;

        private void DrawStrategyInfo(IGraphics graphics)
        {
            if (!this.Debug)
            {
                return;
            }

            int i = 1;

            /* Current Strategies */
            debugArea.DrawTextLine(graphics, i++,
                $"Fire: {this.CurrentFireStrategy?.GetName()}");

            debugArea.DrawTextLine(graphics, i++,
                $"Gather: {this.CurrentGatheringStrategy?.GetName()}");

            debugArea.DrawTextLine(graphics, i++,
                $"Move: {this.CurrentMovementStrategy?.GetName()}");
        }

        private void DrawEnemyInfo(IGraphics graphics)
        {
            if (!this.Debug)
            {
                return;
            }

            if (this.MostPriorityEnemy != null)
            {
                var priortiyBrush = new SolidBrush(Color.FromArgb(150, Color.DarkKhaki));
                var priorityPen = new Pen(priortiyBrush);

                graphics.FillPolygon(priortiyBrush, this.MostPriorityEnemy.HitBox);
                //graphics.DrawArc(
                //    priorityPen,
                //    new RectangleF(
                //        (float)(this.MostPriorityEnemy.Position.X - this.Width / 2.0 - 10),
                //        (float)(this.MostPriorityEnemy.Position.Y - this.Height / 2.0 - 10),
                //        (float)this.Width + 20,
                //        (float)this.Height + 20
                //    ),
                //    0, 360);
                graphics.DrawLine(priorityPen, this.Position, this.MostPriorityEnemy.Position);


                var brush = new SolidBrush(Color.FromArgb(75, Color.OrangeRed));

                foreach (var enemy in this.EnemiesAlive.Except(new[] { this.MostPriorityEnemy }))
                {
                    graphics.FillPolygon(brush, enemy.HitBox);

                }
            }
        }

        private void DrawEnemyBulletInfo(IGraphics graphics)
        {
            if (!this.ShowBulletWaves || !this.Debug)
            {
                return;
            }

            var brush = new SolidBrush(Color.FromArgb(75, Color.Orange));
            var pen = new Pen(brush);

            foreach (var bullet in this.EnemiesAlive.SelectMany(e => e.ActiveBullets))
            {
                var r = (float)bullet.DistanceTravaled;
                var start = new PointF(
                    bullet.FiredPosition.X - r,
                    bullet.FiredPosition.Y - r);

                var wallPoint = MathUtils.GetWallPoint(
                    450,
                    this.DistanceFarestCorner,
                    bullet.Heading,
                    bullet.CurrentPosition,
                    this.BattleFieldWidth,
                    this.BattleFieldHeight);

                graphics.FillEllipse(brush, new RectangleF(
                    bullet.FiredPosition.X - 3,
                    bullet.FiredPosition.Y - 3,
                    7,
                    7));

                graphics.DrawArc(pen, new RectangleF(
                    bullet.CurrentPosition.X - 3,
                    bullet.CurrentPosition.Y - 3,
                    7,
                    7),
                    0,
                    360);

                graphics.DrawArc(pen, new RectangleF(start, new SizeF(2 * r, 2 * r)), 0, 360);
                graphics.DrawLine(pen, bullet.FiredPosition, wallPoint);
            }
        }
#endif
    }
}