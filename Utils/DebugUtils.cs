﻿using System.Drawing;
using Robocode;

namespace PK.Robocode.Core.Utils
{
    public class DebugArea
    {
        public DebugArea(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Color Color { get; set; } = Color.FromArgb(150, Color.WhiteSmoke);

        protected SolidBrush Brush => new SolidBrush(Color);

        public Font Font { get; set; } = new Font(FontFamily.GenericMonospace, 5f);

        public int Margin { get; set; } = 5;

        public int FontSize { get; set; } = 15;

        public double X { get; set; }

        public double Y { get; set; }

        public void DrawEmptyLine(IGraphics graphics, int index)
        {
            this.DrawTextLine(graphics, index, string.Empty);
        }

        public void DrawTextLine(IGraphics graphics, int index, string text)
        {
            if (text == null)
            {
                text = string.Empty;
            }

            graphics.DrawString(
                text,
                Font,
                Brush,
                new PointF(
                    (float)(this.X + this.Margin),
                    (float)(this.Y - (this.Margin + this.FontSize) * index)));
        }
    }
}
