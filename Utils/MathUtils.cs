﻿using RoboUtils = Robocode.Util;

namespace PK.Robocode.Core.Utils
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Linq;

    public static class MathUtils
    {
        public static bool IsNear(double d1, double d2)
        {
            return RoboUtils.Utils.IsNear(d1, d2);
        }

        public static double NormalAbsoluteAngle(double angle)
        {
            return RoboUtils.Utils.NormalAbsoluteAngle(angle);
        }

        public static double NormalAbsoluteAngleDegrees(double angle)
        {
            return RoboUtils.Utils.NormalAbsoluteAngleDegrees(angle);
        }

        public static double NormalNearAbsoluteAngle(double angle)
        {
            return RoboUtils.Utils.NormalNearAbsoluteAngle(angle);
        }

        public static double NormalNearAbsoluteAngleDegrees(double angle)
        {
            return RoboUtils.Utils.NormalNearAbsoluteAngleDegrees(angle);
        }

        public static double NormalRelativeAngle(double angle)
        {
            return RoboUtils.Utils.NormalRelativeAngle(angle);
        }

        public static double NormalRelativeAngleDegrees(double angle)
        {
            return RoboUtils.Utils.NormalRelativeAngleDegrees(angle);
        }

        public static double AbsoluteBearing(PointF source, PointF target)
        {
            return Math.Atan2(target.X - source.X, target.Y - source.Y);
        }

        public static double AbsoluteBearingRadians(PointF source, PointF target)
        {
            return DegreeToRadian(AbsoluteBearing(source, target));
        }

        public static double NormilizeDiff(double v1, double v2, double sourceMin, double sourceMax)
        {
            var diff = MapNumber(Math.Abs(v1 - v2), sourceMin, sourceMax, 0, 1);

            return diff;
        }

        public static Random GetRandom()
        {
            return new Random();
        }

        public static double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        public static double DegreeToRadian(double angle)
        {
            return Math.PI * (angle % 360) / 180.0;
        }

        public static PointF PointOnCircle(double radius, double angleInDegrees, PointF origin)
        {
            // Convert from degrees to radians via multiplication by PI/180
            var x = (float)(radius * Math.Cos(angleInDegrees % 360 * Math.PI / 180F)) + origin.X;
            var y = (float)(radius * Math.Sin(angleInDegrees % 360 * Math.PI / 180F)) + origin.Y;

            return new PointF(x, y);
        }

        public static double DistanceBetween(PointF p1, PointF p2)
        {
            return Math.Sqrt(Math.Pow(Math.Abs(p1.X - p2.X), 2) + Math.Pow(Math.Abs(p1.Y - p2.Y), 2));
        }

        public static double DistanceBetween(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(Math.Abs(x1 - x2), 2) + Math.Pow(Math.Abs(y1 - y2), 2));
        }

        public static void ShrinkToBattleField(ref PointF point, double maxX, double maxY, double x, double y)
        {
            if (point.X > maxX)
            {
                var z = (maxX - x) * Math.Abs(point.Y - y) / (point.X - x);
                point.Y = (float)(point.Y > y ? y + z : y - z);

                point.X = (float)maxX;
            }

            else if (point.X < 0)
            {
                var z = x * Math.Abs(point.Y - y) / (Math.Abs(point.X) + x);
                point.Y = (float)(point.Y > y ? y + z : y - z);

                point.X = 0;
            }

            if (point.Y > maxY)
            {
                var z = (maxY - y) * Math.Abs(point.X - x) / (point.Y - y);
                point.X = (float)(point.X > x ? x + z : x - z);

                point.Y = (float)maxY;
            }

            else if (point.Y < 0)
            {
                var z = y * Math.Abs(point.X - x) / (Math.Abs(point.Y) + y);
                point.X = (float)(point.X > x ? x + z : x - z);

                point.Y = 0;
            }
        }

        public static double TurnSpeed(double velocity)
        {
            return 10 - 0.75 * Math.Abs(velocity);
        }

        public static PointF GetFuturePosition(IRobotMeta meta, long duration)
        {
            return GetFuturePosition(meta.Position, meta.Velocity, meta.Heading, duration);
        }

        public static PointF GetFuturePosition(PointF startPosition, double velocity, double heading, long duration)
        {
            return PointOnCircle((float)velocity * duration, (float)(450 - heading) % 360, startPosition);
        }

        public static PointF[] GetHitbox(IRobotMeta robot)
        {
            var radius = (float)Math.Sqrt(
                          Math.Pow(Definitions.CollissionBoxWidth / 2.0, 2)
                        + Math.Pow(Definitions.CollissionBoxHeight / 2.0, 2));

            var angle = (float)RadianToDegree(
                    Math.Atan(Definitions.CollissionBoxHeight / 2.0 / (Definitions.CollissionBoxWidth / 2.0)));

            return new[]
                       {
                            PointOnCircle(radius, 450 - (float)robot.Heading - angle, robot.Position),
                            PointOnCircle(radius, 450 - (float)robot.Heading + angle, robot.Position),
                            PointOnCircle(radius, 450 - (float)robot.Heading - angle + 180, robot.Position),
                            PointOnCircle(radius, 450 - (float)robot.Heading + angle + 180, robot.Position)
                        };
        }

        public static double RotationRate(double velocity)
        {
            return 10 - 0.75 * Math.Abs(velocity);
        }

        public static double BulletVelocity(double power)
        {
            return 20 - 3 * power;
        }

        public static double EnergyGain(double power)
        {
            return 3 * power;
        }

        public static double TimeUntilFireAgain(double power)
        {
            return Math.Ceiling((1 + power / 5) * 10);
        }

        public static double BulletDamage(double power)
        {
            return power * 4 + Math.Max(0, power - 1) * 2;
        }

        public static double BulletPower(double damage)
        {
            return damage <= 4.0
                ? damage / 4.0
                : damage * 2.0 / 6.0;
        }

        public static double GunHeatup(double power)
        {
            return 1 + power / 5;
        }

        //public static double AntiBearingTo(PointF p1, PointF p2)
        //{
        //    var distanceX = Math.Abs(p1.X - p2.X);
        //    var distanceY = Math.Abs(p1.Y - p2.Y);
        //    var distance = DistanceBetween(p1, p2);

        //    // upper right
        //    if (p1.X < p2.X && p1.Y < p2.Y)
        //    {
        //        return 270 - RadianToDegree(Math.Asin(distanceX / distance));
        //    }

        //    // lower right
        //    if (p1.X < p2.X && p1.Y > p2.Y)
        //    {
        //        return 180 - RadianToDegree(Math.Asin(distanceY / distance));
        //    }

        //    // lower left
        //    if (p1.X > p2.X && p1.Y > p2.Y)
        //    {
        //        return 90 - RadianToDegree(Math.Asin(distanceX / distance));
        //    }

        //    // upper left
        //    if (p1.X > p2.X && p1.Y < p2.Y)
        //    {
        //        var degree = RadianToDegree(Math.Asin(distanceY / distance));
        //        return 360 - degree;
        //    }

        //    return 0.0;
        //}

        public static double Bearing(PointF p1, PointF p2)
        {
            var distanceX = Math.Abs(p1.X - p2.X);
            var distanceY = Math.Abs(p1.Y - p2.Y);
            var distance = DistanceBetween(p1, p2);

            // upper right
            if (p1.X < p2.X && p1.Y < p2.Y)
            {
                return 90 - RadianToDegree(Math.Asin(distanceY / distance));
            }

            // lower right
            if (p1.X < p2.X && p1.Y > p2.Y)
            {
                return 180 - RadianToDegree(Math.Asin(distanceX / distance));
            }

            // lower left
            if (p1.X > p2.X && p1.Y > p2.Y)
            {
                return 270 - RadianToDegree(Math.Asin(distanceY / distance));
            }

            // upper left
            if (p1.X > p2.X && p1.Y < p2.Y)
            {
                var degree = RadianToDegree(Math.Asin(distanceX / distance));
                return 360 - degree;
            }

            return 0.0;
        }

        public static double Bearing(double x1, double y1, double x2, double y2)
        {
            return Bearing(new PointF((float)x1, (float)y1), new PointF((float)x2, (float)y2));
        }

        public static double BearingRadians(PointF p1, PointF p2)
        {
            return DegreeToRadian(Bearing(p1, p2));
        }

        public static double BearingRadians(double x1, double y1, double x2, double y2)
        {
            return DegreeToRadian(Bearing(x1, y1, x2, y2));
        }

        public static long BulletTravelTime(double firepower, double distance)
        {
            return (long)(distance / BulletVelocity(firepower));
        }

        public static bool IsWithinBattlefield(PointF point, double battlefieldWidth, double battlefieldHeight)
        {
            return !(point.X >= battlefieldWidth) && !(point.X < 0) && !(point.Y >= battlefieldHeight) && !(point.Y < 0);
        }

        public static double Limit(double min, double value, double max)
        {
            return Math.Max(min, Math.Min(value, max));
        }

        public static PointF Project(PointF source, double angle, double length)
        {
            return new PointF(
                (float)(source.X + Math.Sin(angle) * length),
                (float)(source.Y + Math.Cos(angle) * length));
        }

        public static double MaxEscapeAngle(double velocity)
        {
            return Math.Asin(8.0 / velocity);
        }

        public static void DrawRotatedRectangle(Graphics g, Rectangle r, float angle)
        {
            using (var m = new Matrix())
            {
                m.RotateAt(angle, new PointF(r.Left + r.Width / 2, r.Top + r.Height / 2));
                g.Transform = m;
                g.DrawRectangle(Pens.Black, r);
                g.ResetTransform();
            }
        }

        public static bool IsInPolygon(Point[] poly, Point point)
        {
            var coef =
                poly.Skip(1)
                    .Select(
                        (p, i) => (point.Y - poly[i].Y) * (p.X - poly[i].X) - (point.X - poly[i].X) * (p.Y - poly[i].Y))
                    .ToList();

            if (coef.Any(p => p == 0))
            {
                return true;
            }

            for (var i = 1; i < coef.Count(); i++)
            {
                if (coef[i] * coef[i - 1] < 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsBetween(double v1, double lower, double upper)
        {
            return v1 > lower && v1 < upper;
        }

        public static PointF GetWallPoint(
            double headingDistance,
            double farestCornerDistance,
            double heading,
            PointF position,
            double battlefieldWidth,
            double battlefieldHeight)
        {
            var point = PointOnCircle(farestCornerDistance, headingDistance - heading, position);

            ShrinkToBattleField(
                ref point,
                (float)battlefieldWidth,
                (float)battlefieldHeight,
                position.X,
                position.Y);

            return point;
        }

        public static double MapNumber(double value, double sourceMin, double sourceMax, double destinationMin, double destinationMax)
        {
            // (X-A)/(B-A) * (D-C) + C
            return (value - sourceMin) / (sourceMax - sourceMin) * (destinationMax - destinationMin) + destinationMin;
        }

        public static double MapLimitedNumber(double value, double sourceMin, double sourceMax, double destinationMin, double destinationMax)
        {
            var limit = Limit(sourceMin, value, sourceMax);

            return MapNumber(limit, sourceMin, sourceMax, destinationMin, destinationMax);
        }
    }
}