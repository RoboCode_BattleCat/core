﻿namespace PK.Robocode.Core.RobotExtensions
{
    public interface IRobotExtension
    {
        bool IsActive { get; }

        void Reset();

        void Activate();

        void Deactivate();

        bool Debug { get; set; }
    }
}
