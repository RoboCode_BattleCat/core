namespace PK.Robocode.Core.RobotExtensions
{
    public abstract class AbstractRobotExtension : IRobotExtension
    {
        public AbstractRobotExtension(AbstractCoreRobot robot)
        {
            this.Robot = robot;

#if DEBUG
            this.Robot.Draw += this.Draw;
#endif
        }

        public bool Debug { get; set; } = false;

        protected AbstractCoreRobot Robot { get; set; }

        public bool IsActive { get; protected set; } = true;

        public void Activate()
        {
            this.IsActive = true;

            this.Activated();
        }

        protected virtual void Activated()
        {
            // Do nothing by default
        }

        public void Deactivate()
        {
            this.IsActive = false;

            this.Deactivated();
        }

        protected virtual void Deactivated()
        {
            // Do nothing by default
        }

        /// <summary>
        /// Does nothing by default
        /// </summary>
        public virtual void Reset()
        {
        }

#if DEBUG
        private void Draw(object sender, IGraphics graphics)
        {
            if (this.Debug && this.IsActive)
            {
                this.Print(graphics);
            }
        }

        protected virtual void Print(IGraphics graphics)
        {
            // do nothing by default
        }
#endif
    }
}
