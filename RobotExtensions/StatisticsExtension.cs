﻿using System;
using System.Collections.Generic;
using System.Linq;
using PK.Robocode.Core.Model;
using PK.Robocode.Core.Strategies.Fire;
using PK.Robocode.Core.Utils;
using Robocode;

namespace PK.Robocode.Core.RobotExtensions
{
    public class StatisticsExtension : AbstractRobotExtension
    {
        public StatisticsExtension(AbstractCoreRobot robot) : base(robot)
        {
            this.CurrentStatistic = new RobotStatistic { Name = this.Robot.Name };

            this.Activated();

#if DEBUG
            this.debugArea = new DebugArea(5, this.Robot.BattleFieldHeight - 150);
#endif
        }

        public RobotStatistic CurrentStatistic { get; protected set; }

        public static IList<IDictionary<string, RobotStatistic>> RoundStatistics { get; protected set; } =
            new List<IDictionary<string, RobotStatistic>>();

        public IDictionary<string, RobotStatistic> CurrentEnemyStatistics { get; protected set; } =
            new Dictionary<string, RobotStatistic>();

        public IDictionary<string, RobotStatistic> AverageRoundStatistics
        {
            get
            {
                var averageStatistic = new Dictionary<string, RobotStatistic>();

                foreach (var group in RoundStatistics.SelectMany(s => s.Values).GroupBy(s => s.Name))
                {
                    averageStatistic.Add(
                        group.Key,
                        new RobotStatistic
                        {
                            Name = group.Key,
                            BulletHitBullet = group.Average(g => g.BulletHitBullet),
                            DamageTaken = group.Average(g => g.DamageTaken),
                            DamgeDealed = group.Average(g => g.DamgeDealed),
                            FireCount = group.Average(g => g.FireCount),
                            HitByBulletCount = group.Average(g => g.HitByBulletCount),
                            BulletMissedCount = group.Average(g => g.BulletMissedCount),
                            BulletHitCount = group.Average(g => g.BulletHitCount)
                        });
                }

                return averageStatistic;
            }
        }

        public IDictionary<string, RobotStatistic> LastRoundStatistic => RoundStatistics.Last();

        private void Fired(object sender, Tuple<IFireStrategy, Model.Bullet> args)
        {
            this.CurrentStatistic.FireCount++;
        }

        protected override void Activated()
        {
            this.Robot.RoundEnded += this.RoundEnded;
            this.Robot.HitByBullet += this.HitByBullet;
            this.Robot.BulletMissed += this.BulletMissed;
            this.Robot.BulletHitBullet += this.BulletHitBullet;
            this.Robot.BulletHit += this.BulletHit;
            this.Robot.Fired += this.Fired;
        }

        protected override void Deactivated()
        {
            this.Robot.RoundEnded -= this.RoundEnded;
            this.Robot.HitByBullet -= this.HitByBullet;
            this.Robot.BulletMissed -= this.BulletMissed;
            this.Robot.BulletHitBullet -= this.BulletHitBullet;
            this.Robot.BulletHit -= this.BulletHit;
            this.Robot.Fired -= this.Fired;
        }

        private void BulletHit(object sender, IBullet bullet)
        {
            if (bullet.Victim != null)
            {

                if (!this.CurrentEnemyStatistics.ContainsKey(bullet.Victim?.Name))
                {
                    this.CurrentEnemyStatistics.Add(bullet.Victim.Name, new RobotStatistic { Name = bullet.Victim.Name });
                }

                this.CurrentEnemyStatistics[bullet.Victim.Name].HitByBulletCount++;
            }

            this.CurrentStatistic.BulletHitCount++;

            var damage = MathUtils.BulletDamage(bullet.Power);

            this.CurrentStatistic.DamgeDealed += damage;
            this.CurrentEnemyStatistics[bullet.Victim.Name].DamageTaken += damage;
        }

        private void BulletHitBullet(object sender, Tuple<IBullet, IBullet> args)
        {
            if (args.Item2.FiredBy != null)
            {
                if (!this.CurrentEnemyStatistics.ContainsKey(args.Item2.FiredBy.Name))
                {
                    this.CurrentEnemyStatistics.Add(args.Item2.FiredBy.Name, new RobotStatistic { Name = args.Item2.FiredBy.Name });
                }

                this.CurrentEnemyStatistics[args.Item2.FiredBy.Name].BulletHitBullet++;
                this.CurrentEnemyStatistics[args.Item2.FiredBy.Name].BulletMissedCount++;
            }

            this.CurrentStatistic.BulletHitBullet++;
            this.CurrentStatistic.BulletMissedCount++;
        }

        private void BulletMissed(object sender, IBullet bullet)
        {
            this.CurrentStatistic.BulletMissedCount++;
        }

        private void HitByBullet(object sender, IBullet bullet)
        {
            if (bullet.FiredBy == null)
            {
                return;
            }

            if (!this.CurrentEnemyStatistics.ContainsKey(bullet.FiredBy.Name))
            {
                this.CurrentEnemyStatistics.Add(bullet.FiredBy.Name, new RobotStatistic { Name = bullet.FiredBy.Name });
            }

            this.CurrentEnemyStatistics[bullet.FiredBy.Name].BulletHitCount++;
            this.CurrentStatistic.HitByBulletCount++;

            var damage = MathUtils.BulletDamage(bullet.Power);
            this.CurrentEnemyStatistics[bullet.FiredBy.Name].DamgeDealed += damage;
            this.CurrentStatistic.DamageTaken += damage;
        }

        private void RoundEnded(object sender, RoundEndedEvent e)
        {
            RoundStatistics.Add(new Dictionary<string, RobotStatistic>(this.CurrentEnemyStatistics));

            this.CurrentEnemyStatistics = new Dictionary<string, RobotStatistic>();
            this.CurrentStatistic = new RobotStatistic { Name = this.Robot.Name };
        }

#if DEBUG
        private DebugArea debugArea;


        protected override void Print(IGraphics graphics)
        {
            var i = 1;

            this.debugArea.DrawEmptyLine(graphics, i++);

            this.debugArea.DrawTextLine(graphics, i++,
                $"Accurency: {Math.Round(CurrentStatistic.Accurency, 2)}%");

            this.debugArea.DrawTextLine(graphics, i++,
                $"FireCount: {Math.Round(CurrentStatistic.FireCount, 2)}");

            this.debugArea.DrawTextLine(graphics, i++,
                $"BulletHitCount: {Math.Round(CurrentStatistic.BulletHitCount, 2)}");

            this.debugArea.DrawTextLine(graphics, i++,
                $"BulletMissedCount: {Math.Round(CurrentStatistic.BulletMissedCount, 2)}");

            this.debugArea.DrawTextLine(graphics, i++,
                $"BulletHitBulletCount: {Math.Round(CurrentStatistic.BulletHitBullet, 2)}");

            this.debugArea.DrawTextLine(graphics, i++,
                $"DamageTaken: {Math.Round(CurrentStatistic.DamageTaken, 2)}");

            this.debugArea.DrawTextLine(graphics, i++,
                $"DamgeDealed: {Math.Round(CurrentStatistic.DamgeDealed, 2)}");

            var mostPriorEnemy = this.Robot.MostPriorityEnemy;

            if (mostPriorEnemy != null && CurrentEnemyStatistics.ContainsKey(mostPriorEnemy.Name))
            {
                var enemy = CurrentEnemyStatistics[mostPriorEnemy.Name];

                if (enemy != null)
                {
                    this.debugArea.DrawEmptyLine(graphics, i++);

                    this.debugArea.DrawTextLine(graphics, i++,
                        $"Priority Enemy: {enemy.Name}");

                    //this.debugArea.DrawTextLine(graphics, i++,
                    //    $"Accurency: {MathUtils.Round(enemy.Accurency, 2)}%");

                    //this.debugArea.DrawTextLine(graphics, i++,
                    //    $"FireCount: {MathUtils.Round(enemy.FireCount, 2)}");

                    //this.debugArea.DrawTextLine(graphics, i++,
                    //    $"BulletHitCount: {MathUtils.Round(enemy.BulletHitCount, 2)}");

                    //this.debugArea.DrawTextLine(graphics, i++,
                    //    $"BulletMissedCount: {MathUtils.Round(enemy.BulletMissedCount, 2)}");

                    //this.debugArea.DrawTextLine(graphics, i++,
                    //    $"BulletHitBulletCount: {MathUtils.Round(enemy.BulletHitBullet, 2)}");

                    this.debugArea.DrawTextLine(graphics, i++,
                        $"DamageTaken: {Math.Round(enemy.DamageTaken, 2)}");

                    this.debugArea.DrawTextLine(graphics, i++,
                        $"DamgeDealed: {Math.Round(enemy.DamgeDealed, 2)}");
                }
            }

            var enemies = CurrentEnemyStatistics.Values.OrderBy(e => e.DamageTaken - e.DamgeDealed);
            if (enemies.Any())
            {
                /* Empty Line */
                this.debugArea.DrawEmptyLine(graphics, i++);

                foreach (var e in enemies)
                {
                    this.debugArea.DrawTextLine(graphics, i++,
                        $"{e.Name}: {Math.Round(e.DamageTaken - e.DamgeDealed, 2)}");
                }
            }
        }
#endif
    }
}
