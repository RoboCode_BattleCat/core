﻿using System;

namespace PK.Robocode.Core.Extra.Cache
{
    public class Once<T> : IPropertyCache<T>
    {
        private T value;
        public bool IsSet { get; private set; } = false;

        public Func<T> Func { get; private set; }

        public Once(Func<T> func)
        {
            this.Func = func;
        }

        public T Value
        {
            get
            {
                if (!IsSet)
                {
                    IsSet = true;
                    value = this.Func();
                }

                return value;
            }
        }

        public void Refresh()
        {
            this.IsSet = false;
        }
    }
}
