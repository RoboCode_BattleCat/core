﻿namespace PK.Robocode.Core.Extra.Cache
{
    public interface IDependendPropertyCache<T, D>
    {
        T Value(D dependency);

        void Refresh();
    }
}
