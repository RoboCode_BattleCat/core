﻿namespace PK.Robocode.Core.Extra.Cache
{
    public interface IPropertyCache<T>
    {
        T Value { get; }

        void Refresh();
    }
}
