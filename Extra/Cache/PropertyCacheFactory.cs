﻿using System;

namespace PK.Robocode.Core.Extra.Cache
{
    public static class PropertyCacheFactory
    {
        public static IPropertyCache<T> Create<T>(Func<T> func, bool once = false)
        {
            return once
                ? new Once<T>(func) as IPropertyCache<T>
                : new OncePerTick<T>(func) as IPropertyCache<T>;
        }
    }
}
