﻿using System;

namespace PK.Robocode.Core.Extra.Cache
{
    public class OncePerTick<T> : IPropertyCache<T>
    {
        private long lastUpdate = long.MinValue;
        private T value;

        public int TickOffset { get; }
        public Func<T> Func { get; private set; }

        public OncePerTick(Func<T> func, int tickOffset = 0)
        {
            this.Func = func;
            this.TickOffset = tickOffset;
        }

        public T Value
        {
            get
            {
                var now = AbstractCoreRobot.Instance.Time;

                if ((lastUpdate + this.TickOffset) < now)
                {
                    lastUpdate = now;

                    this.value = this.Func();
                }

                return value;
            }
        }

        public void Refresh()
        {
            this.lastUpdate = -1;
        }
    }
}
