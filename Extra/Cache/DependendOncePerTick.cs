﻿using System;
using System.Collections.Generic;

namespace PK.Robocode.Core.Extra.Cache
{
    public class DependendOncePerTick<T, D> : IDependendPropertyCache<T, D>
    {
        private IDictionary<D, T> values = new Dictionary<D, T>();

        private long lastUpdate = long.MinValue;

        public int TickOffset { get; }
        public Func<D, T> Func { get; private set; }

        public DependendOncePerTick(Func<D, T> func, int tickOffset = 0)
        {
            this.Func = func;
            this.TickOffset = tickOffset;
        }

        public T Value(D dependency)
        {
            if (dependency == null)
            {
                return default(T);
            }

            var now = AbstractCoreRobot.Instance.Time;

            if ((lastUpdate + this.TickOffset) < now)
            {
                lastUpdate = now;
                var value = this.Func(dependency);

                if (this.values.ContainsKey(dependency))
                {
                    this.values[dependency] = value;
                }
                else
                {
                    this.values.Add(dependency, value);
                }
            }

            return this.values[dependency];
        }

        public void Refresh()
        {
            this.lastUpdate = long.MinValue;
        }
    }
}
