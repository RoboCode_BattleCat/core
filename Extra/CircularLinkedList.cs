﻿namespace PK.Robocode.Core.Extra
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    ///     Represents a circular doubly linked list.
    /// </summary>
    /// <typeparam name="T">Specifies the element type of the linked list.</typeparam>
    [DebuggerDisplay("Count = {Count}")]
    public sealed class CircularLinkedList<T> : ICollection<T>, IEnumerable<T>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly IEqualityComparer<T> comparer;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int count;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Node<T> head;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Node<T> tail;

        /// <summary>
        ///     Initializes a new instance of <see cref="CircularLinkedList" />
        /// </summary>
        public CircularLinkedList()
            : this(null, EqualityComparer<T>.Default)
        {
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="CircularLinkedList" />
        /// </summary>
        /// <param name="collection">Collection of objects that will be added to linked list</param>
        public CircularLinkedList(IEnumerable<T> collection)
            : this(collection, EqualityComparer<T>.Default)
        {
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="CircularLinkedList" />
        /// </summary>
        /// <param name="comparer">Comparer used for item comparison</param>
        /// <exception cref="ArgumentNullException">comparer is null</exception>
        public CircularLinkedList(IEqualityComparer<T> comparer)
            : this(null, comparer)
        {
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="CircularLinkedList" />
        /// </summary>
        /// <param name="collection">Collection of objects that will be added to linked list</param>
        /// <param name="comparer">Comparer used for item comparison</param>
        public CircularLinkedList(IEnumerable<T> collection, IEqualityComparer<T> comparer)
        {
            if (comparer == null)
            {
                throw new ArgumentNullException("comparer");
            }
            this.comparer = comparer;
            if (collection != null)
            {
                foreach (var item in collection)
                {
                    this.AddLast(item);
                }
            }
        }

        /// <summary>
        ///     Gets Tail node. Returns NULL if no tail node found
        /// </summary>
        public Node<T> Tail
        {
            get
            {
                return this.tail;
            }
        }

        /// <summary>
        ///     Gets the head node. Returns NULL if no node found
        /// </summary>
        public Node<T> Head
        {
            get
            {
                return this.head;
            }
        }

        /// <summary>
        ///     Gets the item at the current index
        /// </summary>
        /// <param name="index">Zero-based index</param>
        /// <exception cref="ArgumentOutOfRangeException">index is out of range</exception>
        public Node<T> this[int index]
        {
            get
            {
                if (index >= this.count || index < 0)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                var node = this.head;
                for (var i = 0; i < index; i++)
                {
                    node = node.Next;
                }
                return node;
            }
        }

        /// <summary>
        ///     Gets total number of items in the list
        /// </summary>
        public int Count
        {
            get
            {
                return this.count;
            }
        }

        /// <summary>
        ///     Removes the first occurance of the supplied item
        /// </summary>
        /// <param name="item">Item to be removed</param>
        /// <returns>TRUE if removed, else FALSE</returns>
        public bool Remove(T item)
        {
            // finding the first occurance of this item
            var nodeToRemove = this.Find(item);
            if (nodeToRemove != null)
            {
                return this.RemoveNode(nodeToRemove);
            }
            return false;
        }

        /// <summary>
        ///     Clears the list
        /// </summary>
        public void Clear()
        {
            this.head = null;
            this.tail = null;
            this.count = 0;
        }

        /// <summary>
        ///     Gets a forward enumerator
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            var current = this.head;
            if (current != null)
            {
                do
                {
                    yield return current.Value;
                    current = current.Next;
                }
                while (current != this.head);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        ///     Determines whether a value is in the list.
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns>TRUE if item exist, else FALSE</returns>
        public bool Contains(T item)
        {
            return this.Find(item) != null;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }
            if (arrayIndex < 0 || arrayIndex > array.Length)
            {
                throw new ArgumentOutOfRangeException("arrayIndex");
            }

            var node = this.head;
            do
            {
                array[arrayIndex++] = node.Value;
                node = node.Next;
            }
            while (node != this.head);
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        bool ICollection<T>.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        void ICollection<T>.Add(T item)
        {
            this.AddLast(item);
        }

        /// <summary>
        ///     Add a new item to the end of the list
        /// </summary>
        /// <param name="item">Item to be added</param>
        public void AddLast(T item)
        {
            // if head is null, then this will be the first item
            if (this.head == null)
            {
                this.AddFirstItem(item);
            }
            else
            {
                var newNode = new Node<T>(item);
                this.tail.Next = newNode;
                newNode.Next = this.head;
                newNode.Previous = this.tail;
                this.tail = newNode;
                this.head.Previous = this.tail;
            }
            ++this.count;
        }

        private void AddFirstItem(T item)
        {
            this.head = new Node<T>(item);
            this.tail = this.head;
            this.head.Next = this.tail;
            this.head.Previous = this.tail;
        }

        /// <summary>
        ///     Adds item to the last
        /// </summary>
        /// <param name="item">Item to be added</param>
        public void AddFirst(T item)
        {
            if (this.head == null)
            {
                this.AddFirstItem(item);
            }
            else
            {
                var newNode = new Node<T>(item);
                this.head.Previous = newNode;
                newNode.Previous = this.tail;
                newNode.Next = this.head;
                this.tail.Next = newNode;
                this.head = newNode;
            }
            ++this.count;
        }

        /// <summary>
        ///     Adds the specified item after the specified existing node in the list.
        /// </summary>
        /// <param name="node">Existing node after which new item will be inserted</param>
        /// <param name="item">New item to be inserted</param>
        /// <exception cref="ArgumentNullException"><paramref name="node" /> is NULL</exception>
        /// <exception cref="InvalidOperationException"><paramref name="node" /> doesn't belongs to list</exception>
        public void AddAfter(Node<T> node, T item)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            // ensuring the supplied node belongs to this list
            var temp = this.FindNode(this.head, node.Value);
            if (temp != node)
            {
                throw new InvalidOperationException("Node doesn't belongs to this list");
            }

            var newNode = new Node<T>(item);
            newNode.Next = node.Next;
            node.Next.Previous = newNode;
            newNode.Previous = node;
            node.Next = newNode;

            // if the node adding is tail node, then repointing tail node
            if (node == this.tail)
            {
                this.tail = newNode;
            }
            ++this.count;
        }

        /// <summary>
        ///     Adds the new item after the specified existing item in the list.
        /// </summary>
        /// <param name="existingItem">Existing item after which new item will be added</param>
        /// <param name="newItem">New item to be added to the list</param>
        /// <exception cref="ArgumentException"><paramref name="existingItem" /> doesn't exist in the list</exception>
        public void AddAfter(T existingItem, T newItem)
        {
            // finding a node for the existing item
            var node = this.Find(existingItem);
            if (node == null)
            {
                throw new ArgumentException("existingItem doesn't exist in the list");
            }
            this.AddAfter(node, newItem);
        }

        /// <summary>
        ///     Adds the specified item before the specified existing node in the list.
        /// </summary>
        /// <param name="node">Existing node before which new item will be inserted</param>
        /// <param name="item">New item to be inserted</param>
        /// <exception cref="ArgumentNullException"><paramref name="node" /> is NULL</exception>
        /// <exception cref="InvalidOperationException"><paramref name="node" /> doesn't belongs to list</exception>
        public void AddBefore(Node<T> node, T item)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }
            // ensuring the supplied node belongs to this list
            var temp = this.FindNode(this.head, node.Value);
            if (temp != node)
            {
                throw new InvalidOperationException("Node doesn't belongs to this list");
            }

            var newNode = new Node<T>(item);
            node.Previous.Next = newNode;
            newNode.Previous = node.Previous;
            newNode.Next = node;
            node.Previous = newNode;

            // if the node adding is head node, then repointing head node
            if (node == this.head)
            {
                this.head = newNode;
            }
            ++this.count;
        }

        /// <summary>
        ///     Adds the new item before the specified existing item in the list.
        /// </summary>
        /// <param name="existingItem">Existing item before which new item will be added</param>
        /// <param name="newItem">New item to be added to the list</param>
        /// <exception cref="ArgumentException"><paramref name="existingItem" /> doesn't exist in the list</exception>
        public void AddBefore(T existingItem, T newItem)
        {
            // finding a node for the existing item
            var node = this.Find(existingItem);
            if (node == null)
            {
                throw new ArgumentException("existingItem doesn't exist in the list");
            }
            this.AddBefore(node, newItem);
        }

        /// <summary>
        ///     Finds the supplied item and returns a node which contains item. Returns NULL if item not found
        /// </summary>
        /// <param name="item">Item to search</param>
        /// <returns><see cref="Node&lt;T&gt;" /> instance or NULL</returns>
        public Node<T> Find(T item)
        {
            var node = this.FindNode(this.head, item);
            return node;
        }

        private bool RemoveNode(Node<T> nodeToRemove)
        {
            var previous = nodeToRemove.Previous;
            previous.Next = nodeToRemove.Next;
            nodeToRemove.Next.Previous = nodeToRemove.Previous;

            // if this is head, we need to update the head reference
            if (this.head == nodeToRemove)
            {
                this.head = nodeToRemove.Next;
            }
            else if (this.tail == nodeToRemove)
            {
                this.tail = this.tail.Previous;
            }

            --this.count;
            return true;
        }

        /// <summary>
        ///     Removes all occurances of the supplied item
        /// </summary>
        /// <param name="item">Item to be removed</param>
        public void RemoveAll(T item)
        {
            var removed = false;
            do
            {
                removed = this.Remove(item);
            }
            while (removed);
        }

        /// <summary>
        ///     Removes head
        /// </summary>
        /// <returns>TRUE if successfully removed, else FALSE</returns>
        public bool RemoveHead()
        {
            return this.RemoveNode(this.head);
        }

        /// <summary>
        ///     Removes tail
        /// </summary>
        /// <returns>TRUE if successfully removed, else FALSE</returns>
        public bool RemoveTail()
        {
            return this.RemoveNode(this.tail);
        }

        private Node<T> FindNode(Node<T> node, T valueToCompare)
        {
            Node<T> result = null;
            if (this.comparer.Equals(node.Value, valueToCompare))
            {
                result = node;
            }
            else if (result == null && node.Next != this.head)
            {
                result = this.FindNode(node.Next, valueToCompare);
            }
            return result;
        }

        /// <summary>
        ///     Gets a reverse enumerator
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetReverseEnumerator()
        {
            var current = this.tail;
            if (current != null)
            {
                do
                {
                    yield return current.Value;
                    current = current.Previous;
                }
                while (current != this.tail);
            }
        }

        /// <summary>
        ///     Represents a node
        /// </summary>
        /// <typeparam name="E"></typeparam>
        [DebuggerDisplay("Value = {Value}")]
        public sealed class Node<E>
        {
            /// <summary>
            ///     Initializes a new <see cref="Node" /> instance
            /// </summary>
            /// <param name="item">Value to be assigned</param>
            internal Node(E item)
            {
                this.Value = item;
            }

            /// <summary>
            ///     Gets the Value
            /// </summary>
            public E Value { get; }

            /// <summary>
            ///     Gets next node
            /// </summary>
            public Node<E> Next { get; internal set; }

            /// <summary>
            ///     Gets previous node
            /// </summary>
            public Node<E> Previous { get; internal set; }
        }
    }
}