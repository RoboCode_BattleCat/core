﻿namespace PK.Robocode.Core.Extra
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;

    public class CircularBuffer<T> : ICollection<T>, IEnumerable<T>, ICollection, IEnumerable
    {
        private T[] buffer;

        private int capacity;

        private int head;

        [NonSerialized]
        private object syncRoot;

        private int tail;

        public CircularBuffer(int capacity)
            : this(capacity, false)
        {
        }

        public CircularBuffer(int capacity, bool allowOverflow)
        {
            if (capacity < 0)
            {
                throw new ArgumentException("capacity must be greater than or equal to zero.", nameof(capacity));
            }

            this.capacity = capacity;
            this.Size = 0;
            this.head = 0;
            this.tail = 0;
            this.buffer = new T[capacity];
            this.AllowOverflow = allowOverflow;
        }

        public bool AllowOverflow { get; set; }

        public int Capacity
        {
            get
            {
                return this.capacity;
            }
            set
            {
                if (value == this.capacity)
                {
                    return;
                }

                if (value < this.Size)
                {
                    throw new ArgumentOutOfRangeException(
                        nameof(value),
                        "value must be greater than or equal to the buffer size.");
                }

                var dst = new T[value];
                if (this.Size > 0)
                {
                    this.CopyTo(dst);
                }
                this.buffer = dst;

                this.capacity = value;
            }
        }

        public int Size { get; private set; }

        public T Head => this.buffer[this.head];

        public bool Contains(T item)
        {
            var bufferIndex = this.head;
            var comparer = EqualityComparer<T>.Default;
            for (var i = 0; i < this.Size; i++, bufferIndex++)
            {
                if (bufferIndex == this.capacity)
                {
                    bufferIndex = 0;
                }

                if (item == null && this.buffer[bufferIndex] == null)
                {
                    return true;
                }
                if ((this.buffer[bufferIndex] != null) && comparer.Equals(this.buffer[bufferIndex], item))
                {
                    return true;
                }
            }

            return false;
        }

        public void Clear()
        {
            this.Size = 0;
            this.head = 0;
            this.tail = 0;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.CopyTo(0, array, arrayIndex, this.Size);
        }

        #region IEnumerable<T> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        public int Put(T[] src)
        {
            return this.Put(src, 0, src.Length);
        }

        public int Put(T[] src, int offset, int count)
        {
            var realCount = this.AllowOverflow ? count : Math.Min(count, this.capacity - this.Size);
            var srcIndex = offset;
            for (var i = 0; i < realCount; i++, this.tail++, srcIndex++)
            {
                if (this.tail == this.capacity)
                {
                    this.tail = 0;
                }
                this.buffer[this.tail] = src[srcIndex];
            }
            this.Size = Math.Min(this.Size + realCount, this.capacity);
            return realCount;
        }

        public void Put(T item)
        {
            if (!this.AllowOverflow && this.Size == this.capacity)
            {
                throw new InternalBufferOverflowException("Buffer is full.");
            }

            this.buffer[this.tail] = item;
            if (this.tail++ == this.capacity)
            {
                this.tail = 0;
            }
            this.Size++;
        }

        public void Skip(int count)
        {
            this.head += count;
            if (this.head >= this.capacity)
            {
                this.head -= this.capacity;
            }
        }

        public T[] Get(int count)
        {
            var dst = new T[count];
            this.Get(dst);
            return dst;
        }

        public int Get(T[] dst)
        {
            return this.Get(dst, 0, dst.Length);
        }

        public int Get(T[] dst, int offset, int count)
        {
            var realCount = Math.Min(count, this.Size);
            var dstIndex = offset;
            for (var i = 0; i < realCount; i++, this.head++, dstIndex++)
            {
                if (this.head == this.capacity)
                {
                    this.head = 0;
                }
                dst[dstIndex] = this.buffer[this.head];
            }
            this.Size -= realCount;
            return realCount;
        }

        public T Get()
        {
            if (this.Size == 0)
            {
                throw new InvalidOperationException("Buffer is empty.");
            }

            var item = this.buffer[this.head];
            if (this.head++ == this.capacity)
            {
                this.head = 0;
            }
            this.Size--;
            return item;
        }

        public void CopyTo(T[] array)
        {
            this.CopyTo(array, 0);
        }

        public void CopyTo(int index, T[] array, int arrayIndex, int count)
        {
            if (count > this.Size)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "count cannot be greater than the buffer size.");
            }

            var bufferIndex = this.head;
            for (var i = 0; i < count; i++, bufferIndex++, arrayIndex++)
            {
                if (bufferIndex == this.capacity)
                {
                    bufferIndex = 0;
                }
                array[arrayIndex] = this.buffer[bufferIndex];
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            var bufferIndex = this.head;
            for (var i = 0; i < this.Size; i++, bufferIndex++)
            {
                if (bufferIndex == this.capacity)
                {
                    bufferIndex = 0;
                }

                yield return this.buffer[bufferIndex];
            }
        }

        public T[] GetBuffer()
        {
            return this.buffer;
        }

        public T[] ToArray()
        {
            var dst = new T[this.Size];
            this.CopyTo(dst);
            return dst;
        }

        #region ICollection<T> Members

        int ICollection<T>.Count => this.Size;

        bool ICollection<T>.IsReadOnly => false;

        void ICollection<T>.Add(T item)
        {
            this.Put(item);
        }

        bool ICollection<T>.Remove(T item)
        {
            if (this.Size == 0)
            {
                return false;
            }

            this.Get();
            return true;
        }

        #endregion

        #region ICollection Members

        int ICollection.Count => this.Size;

        bool ICollection.IsSynchronized => false;

        object ICollection.SyncRoot
        {
            get
            {
                if (this.syncRoot == null)
                {
                    Interlocked.CompareExchange(ref this.syncRoot, new object(), null);
                }
                return this.syncRoot;
            }
        }

        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo((T[])array, arrayIndex);
        }

        #endregion
    }
}