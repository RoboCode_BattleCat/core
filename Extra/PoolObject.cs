﻿using System;

namespace PK.Robocode.Core.Extra
{
    public class PoolObject<T> : IDisposable
        where T : PoolObject<T>, new()
    {
        private T internalFoo;
        private Pool<T> pool;

        public PoolObject(Pool<T> pool)
        {
            if (pool == null)
                throw new ArgumentNullException("pool");

            this.pool = pool;
            this.internalFoo = new T();
        }

        public void Dispose()
        {
            if (pool.IsDisposed)
            {
                internalFoo.Dispose();
            }
            else
            {
                pool.Release(this as T);
            }
        }
    }
}
