﻿using System.Collections.Generic;
using PK.Robocode.Core.Model;

namespace PK.Robocode.Core
{
    public interface IRobot : IRobotMeta
    {
        IEnumerable<IBullet> ActiveBullets { get; }

        IEnumerable<IBullet> BulletsHistory { get; }
    }
}
